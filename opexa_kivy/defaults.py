import os
from pathlib import Path

from kivy.metrics import dp

FORM_FIELD_WIDGET_MODULE = 'opexa_kivy.uix.forms.fields.widgets'
FORM_WIDGET_CLASS = 'opexa_kivy.uix.forms.base.OpexaFormWidget'
BASE_POPUP_CLASS = 'opexa_kivy.uix.popup.OpexaPopupWidget'
NOTIFICATION_MANAGER_CLASS = 'opexa_kivy.utils.notification_manager.NotificationManager'

### Database & Synchronization Configuration

WS_TIMEOUT = 30
WS_RECEIVE_TIMEOUT = 300
WS_PREFIX = 'ws'
WS_HOST = '127.0.0.1'
WS_PORT = '8003'
WS_CLIENT_PROTOCOL = 'opexa_kivy.utils.websockets.protocols.SyncClientProtocol'
WS_HEADER_MANAGER = "opexa_kivy.utils.websockets.SecureHeaderManager"

SYNC_SERVER_CLASS = 'opexa_kivy.orm.synchronization.server.SyncServer'
SYNC_WORKER_CLASS = 'opexa_kivy.orm.synchronization.worker.ClientSynchWorker'
SYNC_LOG_PATH = 'sync.log'
SYNC_TIME_ZONE = 'Africa/Harare'

FONT_SM = dp(10)
FONT_MD = dp(15)
FONT_LG = dp(20)
FONT_H4 = dp(23)
FONT_H3 = dp(26)
FONT_H2 = dp(28)
FONT_H1 = dp(30)
LABEL_MD_HEIGHT = dp(60)
MENU_HEIGHT = dp(60)

### Forms ###
FORM_FIELD_HEIGHT = dp(40)

### RecycleViews ###

DEFAULT_DATA_VIEW_HEIGHT = LABEL_MD_HEIGHT


### Popups ###
POPUP_OPEN_DELAY = .2
POPUP_CLOSE_DELAY = -1
POPUP_OPEN_ANIMATION_KWARGS = {
    "duration":0.5, 't':"out_back", "opacity":1
}