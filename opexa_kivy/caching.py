from pathlib import Path
import gzip
import pickle
import asyncio
from collections import defaultdict
from datetime import datetime

from kivy.app import App

from opexa_kivy import applet

from .utils.decorators import fail_gracefully

class PickledCache():
    """ Pickles and writes data to disk. """

    default_value = None

    def __init__(self, *args, cache_id = None, **kwargs):
        if cache_id:
            self.cache_id = cache_id
        self._ensure_path_exists()

    @property
    def default_cache_name(self):
        name = getattr(self, 'cache_id', None)
        if not name:
            raise RuntimeError('No Name!')
            name = self.__class__.__name__.lower() + '.dat'
            return name
        else:
            return name

    @fail_gracefully()
    def _ensure_path_exists(self, path_obj=None):
        """ Makes sure the target path actually exists on the disk"""

        if not path_obj:
            path_obj = self.get_path_to_cache()

        # Make the directory
        if path_obj.is_dir():
            if not path_obj.exists():
                path_obj.mkdir(parents=True, exist_ok=True)

        # Means the path points to a file, so we want to create the
        # parent directories, as well as the file itself
        else:
            if not path_obj.exists():
                path_obj.parents[0].mkdir(parents=True, exist_ok=True)
                asyncio.create_task(self.write(self.default_value))

    @fail_gracefully()
    def get_path_to_cache(self, filename=None):
        if not filename:
            return self.get_relative_path(self.default_cache_name)
        else:
            return self.get_relative_path(filename)

        return Path(path_str)

    def get_relative_path(self, path):
        return Path(applet.settings.BASE_DIR, '_cache', path)

    @fail_gracefully()
    async def cache_reset(self, *args, **kwargs):
        await self.write(self.default_value)

    @fail_gracefully()
    async def read(self, *args, filename=None, **kwargs):

        cache_path = self.get_path_to_cache(filename)
        self._ensure_path_exists(cache_path)

        try:
            with Path(cache_path).open('rb') as f:
                d = f.read()
            return pickle.loads(d)

        except FileNotFoundError:
            pass

        return None

    @fail_gracefully()
    async def write(self, data, *args, filename=None, **kwargs):

        cache_path = self.get_path_to_cache(filename)
        self._ensure_path_exists(cache_path)
        with Path(cache_path).open('wb') as f:
            f.write(pickle.dumps(data))