import json
import re
from pathlib import Path
from itertools import chain

from kivy.core.text import LabelBase
from tiny_applet import settings
from tiny_applet.utils import write_to_log

_register = dict()

def create_fontdict_file(css_fname, output_fname):
    """Creates a font dictionary file. Basically creates a dictionary filled
    with icon_code: unicode_value entries
    obtained from a CSS file.
    :param css_fname: CSS filename where font's rules are declared.
    :param output_fname: Fontd file destination
    """
    with open(css_fname, 'r') as f:
        data = f.read()
        res = _parse(data)
        with open(output_fname, 'w') as o:
            o.write(json.dumps(res))
        return res

def register(name, ttf_fname, fontd_fname):
    """Register an Iconfont
    :param name: font name identifier.
    :param ttf_fname: ttf filename (path)
    :param fontd_fname: fontdic filename. (See create_fontdic)
    """
    font_file = Path(fontd_fname)
    fontd = json.loads(font_file.read_text())
    _register[name] = ttf_fname, fontd_fname, fontd


def icon(code, size=None, color=None, font_name=None):
    """Gets an icon from iconfont.
    :param code: Icon codename (ex: 'icon-name')
    :param size: Icon size
    :param color: Icon color
    :param font_name: Registered font name. If None first one is used.
    :returns: icon text (with markups)
    """
    font = next(iter(_register.keys())) if font_name is None else font_name
    font_data = _register[font]
    try:
        s = "[font=%s]%s[/font]" % (font_data[0], chr(font_data[2][code]))
    except KeyError:
        
        write_to_log(f'Could not locate font "{code}"', level='warning')
        return '[icon-missing]'

    if size is not None:
        s = "[size=%s]%s[/size]" % (size, s)
    if color is not None:
        s = "[color=%s]%s[/color]" % (color, s)

    return s


def register_fa_font(name, ttf_fname:str, fontd_fname:str, **kwargs):
    LabelBase.register(name=name, fn_regular=ttf_fname) # Registers font with Kivy
    if not Path(fontd_fname).exists():
        fontd = ttf_to_fontd(ttf_fname, fontd_fname)
    else:
        fontd = json.loads(Path(fontd_fname).read_text())
    _register[name] = (ttf_fname, fontd_fname, fontd)



def ttf_to_fontd(src, target):
    """Creates a font dictionary file. Basically creates a dictionary filled
    with icon_code: unicode_value entries obtained from a CSS file.
    :param src: TTF where font's rules are declared.
    :param target: Fontd file destination
    """
    from fontTools.ttLib import TTFont
    from fontTools.unicode import Unicode

    fontd = {}
    ttf = TTFont(src, 0, allowVID=0, ignoreDecompileErrors=True, fontNumber=-1)
    chars = chain.from_iterable([y + (Unicode[y[0]],) for y in x.cmap.items()] for x in ttf["cmap"].tables)
    for font_char in list(chars):
        fontd[font_char[1]] = font_char[0]
    ttf.close()
    with Path(target).open('w') as f:
        json.dump(fontd, f)
    return fontd

    