from functools import partial, wraps
from collections import defaultdict
from copy import copy
import asyncio
import re

from kivy.animation import Animation
from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import StringProperty, ObjectProperty, ListProperty, NumericProperty, ColorProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView
from tiny_applet.orm.decorators import with_db
from tiny_applet.orm.models import ModelBase
from tiny_applet.utils import import_class, write_to_log, fail_gracefully
from tiny_applet import settings
from tiny_applet.errors import FormValidationError

from .utils import field_to_widget
from opexa_kivy.utils import get_color_module
from opexa_kivy.uix.popup import OpexaPopupWidget, SubmitPopup
from .mixins import SaveModelMixin
colors = get_color_module()

Builder.load_string("""

<FormWidget>:
    size_hint_y: None
    spacing: [dp(10), dp(10)]
    padding: dp(5), dp(5)
    height: self.minimum_height

""")


class FormWidget(SaveModelMixin, StackLayout):
    form_class = StringProperty("")
    background_color = ColorProperty(colors.transparent)
    build_delay = NumericProperty(0)
    popup = ObjectProperty()
    initial = ObjectProperty({})
    obj = ObjectProperty()
    obj_class = ObjectProperty()
    font_color = ListProperty([0,0,0,1])
    field_height = NumericProperty(50)
    field_renderers = ObjectProperty({})
    validation_errors = ObjectProperty({})
    post_submit = ObjectProperty()
    async_post_submit = ObjectProperty()
    async_pre_save = ObjectProperty()
    async_post_save = ObjectProperty()
    target = ObjectProperty()

    
    def __init__(self, *args, custom_validator=None, hide_submit=False, **kwargs):
        self.change_cache = {}
        self.hide_submit = hide_submit
        self.custom_validator = custom_validator
        self.submit_padding = dp(20)
        self._fields = []
        self.inputs_ready = False
        self.submit_widget_height = settings.FORM_FIELD_HEIGHT*1.2
        self.validated_form = None
        self.meta_data = {}

        super().__init__(*args, **kwargs)

        self.register_event_type('on_validation_errors')
        self.register_event_type('on_field_widgets_created')
        self.register_event_type('on_form_changed')
        self.register_event_type('on_all_inputs_ready')


    def on_form_class(self, *args, **kwargs):
        Clock.schedule_once(self.build_form, self.build_delay)        


    @property
    def requires_submit(self):
        return not self.hide_submit


    def on_form_changed(self, *args, **kwargs):
        pass



    @property
    def FormClass(self):
        """ Shortcut to retrieve the class of the form (it may be defined by either
        a string or a class). """
        return import_class(self.form_class)

    @property
    def fields(self):
        return self._fields

    @property
    def form_data(self):
        data = {}
        for field_widget in self.get_fields():
            data[field_widget.name] = field_widget.value
        return data

    @property
    def clean_form_data(self):
        """
        By 'clean', we mean that this dictionary should be able to be thrown into an
        INSERT/UPDATE call without throwing any errors.
        """
        ret = {}
        for field_widget in self.get_fields():
            if field_widget.is_null():
                clean_value = field_widget.default_value
            else:
                clean_value = field_widget.value
            ret[field_widget.change_cache_key] = copy(clean_value)
        return ret

        
    @property
    def has_obj(self):
        return self.obj != None

    @property
    def estimated_height(self):
        if not self.hide_submit:
            return self.estimated_height_with_sub

        h = settings.FORM_FIELD_HEIGHT*len(self._form.layout)
        h += self.spacing*(len(self._form.layout)-1)
        return h

    @property
    def estimated_height_with_sub(self):
        h = settings.FORM_FIELD_HEIGHT*len(self._form.layout)
        h += self.submit_widget_height + self.submit_padding
        return h


    async def async_delete(self):
        write_to_log('Warning: No Longer Implemented!', level='warning')


    @fail_gracefully()
    def _input_ready_listener(self, *args, **kwargs):
        field_count = len(list(self.get_fields()))
        current_ready_count = getattr(self, "_ready_input_count", 0)
        current_ready_count += 1
        setattr(self, '_ready_input_count', current_ready_count)
        if current_ready_count == field_count:
            self.dispatch('on_all_inputs_ready')


    @fail_gracefully()
    def build_form(self, *args, **kwargs):
        self._form = self.FormClass(form_widget=self)
        self._form.layout = list([name for name, field in self._form.declared_fields])
        self.make_field_widgets()
        Clock.schedule_once(
            partial(self.dispatch, "on_field_widgets_created"), .05
        )


    def on_field_widgets_created(self, *args, **kwargs):
        pass


    def get_field(self, name):
        for c in self.children:
            if c.name == name:
                return c


    def get_fields(self):
        return self.children


    def get_group(self, members, *args):
        for c in self.children:
            # Determine if the child is a group (GenericField children do not have
            # a "contains_fields" method)
            match_method = getattr(c, "contains_fields", None)
            if match_method:
                if match_method(members):
                    return c


    def get_field_or_group(self, entry):
        if isinstance(entry, tuple):
            return self.get_group(entry)
        else:
            return self.get_field(entry)


    def hide_fields(self, fields, *args):
        for entry in fields:
            widget = self.get_field_or_group(entry)
            if widget:
                widget.hide()

    @fail_gracefully()
    def make_field_widgets(self, *args, **kwargs):

        field_map = {}
        for name, field in self._form.declared_fields:
            field_map[name] = field
        for field_name in self._form.layout:

            field_instance = field_map.get(field_name)

            if not field_instance:
                continue
                
            widget = field_to_widget(
                field_name, field_instance, self,
                obj=self.obj, initial=self.initial
            )

            Clock.schedule_once(
                partial(self.render_field_widget, widget), -1
            )


    def on_all_inputs_ready(self, *args, **kwargs):
        self.inputs_ready = True

    def bind_obj_to_form(self, obj, *args, **kwargs):
        """ Basically, whenever the object changes, we want to update the form's fields
        accordingly. """
        def _bind_callable(*args):
            self.obj = obj
            for field_widget in self.get_fields():
                field_widget.bind_to_obj(obj)
        Clock.schedule_once(_bind_callable, 0)
        

    def on_validation_errors(self, error_dict, form_class_is_valid,
        *args, **kwargs):
        """ Shows a popup alerting the user to the errors """

        # Means the form_class will handle error messages, etc
        if not form_class_is_valid and not error_dict.keys():
            pass

        else:
            popup = ValidationErrorPopup(error_dict=error_dict)
            popup.open(self.to_window(*self.center))


    async def get_instance_kwargs(self, *args, **kwargs):
        
        instance_kwargs = {}
        # Only include form data that corresponds to an actual field
        # on the model class
        field_names = self._form.obj_class.get_field_names()
        for name, v, in self.form_data.items():
            if name in field_names:
                instance_kwargs[name] = v

        return instance_kwargs

    @fail_gracefully()
    def show_fields(self, fields, *args):
        for entry in fields:
            widget = self.get_field_or_group(entry)
            if widget:
                widget.show()

    @fail_gracefully()
    def render_field_widget(self, widget, *args, **kwargs):

        if not widget:
            return

        if widget.name in self.field_renderers:
            render_func = self.field_renderers[widget.name]
            render_func(widget)
        self.add_widget(widget)
        self._fields.append(widget)

    @with_db()
    async def is_valid(self, *args, session=None, silence_errors=False, **kwargs):
        from opexa_kivy.uix.forms.fields.contrib import FormSetFieldWidget
        # Retrieve fields for validation. Should only include visible fields
        if "error_dict" in kwargs:
            error_dict = kwargs["error_dict"]
        else:
            error_dict = defaultdict(list)

        # Perform widget level validation
        for field_widget in self.get_fields():
            if field_widget.is_null():
                continue
            ret = await field_widget.is_valid(
                error_dict=error_dict, session=session
            )

            # If a formset field widget is invalid, then it's own form class
            # will handle triggering the error message, so we can safely exit here
            if not ret and issubclass(field_widget.__class__, FormSetFieldWidget):
                return


        # Perform form class level validation
        form = self.FormClass(form_widget=self)
        form.widget = self
        form_class_is_valid = await form.is_valid(
            session=session, error_dict=error_dict
        )

        if form.errors:
            error_dict = error_dict | form.errors

        if (error_dict or not form_class_is_valid) and silence_errors == False:
            self.dispatch(
                "on_validation_errors", 
                error_dict, form_class_is_valid
            )
            return False
        return True

Builder.load_string("""
#: import icon opexa_kivy.fonts.icon
#: import AcceptButton opexa_kivy.uix.button.AcceptButton

<ValidationErrorPopup>:
    size_hint: (None, None)
    spacing: dp(15)
    padding: [dp(10), dp(15)]

    canvas.after:
        Color:
            rgba: colors.red
        Line:
            rounded_rectangle: self.x, self.y, self.width, self.height, dp(5)



    OpexaLabel:
        text: root.message_text
        font_size: settings.FONT_MD
        color: root.font_color

    AcceptButton:
        popup: root

""")

class ValidationErrorPopup(OpexaPopupWidget):
    message_text = StringProperty("")
    font_color = ColorProperty(colors.black)

    def __init__(self, *args, error_dict={}, **kwargs):
        super().__init__(*args, **kwargs)
        self.message_text = self.error_dict_to_text(error_dict)
    
    @fail_gracefully()
    def error_dict_to_text(self, error_dict, *args, **kwargs):
        ret = ""

        if "required" in error_dict:
            required_str = "The following fields are required:\n\n[b]"
            required_str += "\n".join(
                [x.replace("_", " ").title() for x in error_dict["required"]]
            )
            ret += required_str + "[/b]"

            return ret

        elif "error_message" in error_dict:
            return error_dict["error_message"]