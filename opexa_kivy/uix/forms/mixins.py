from copy import copy
import asyncio

from kivy.clock import Clock
from kivy.properties import ObjectProperty, BooleanProperty
from kivy.app import App
from sqlalchemy import inspect, update, select
from tiny_applet.orm.decorators import with_db
from tiny_applet.decorators import asyncio_thread
from tiny_applet.utils import write_to_log, fail_gracefully
from sqlalchemy.exc import InvalidRequestError


class SaveModelMixin():
    model_class = ObjectProperty()
    disable_autosave = BooleanProperty(False)


    def __init__(self, *args, **kwargs):
        self._saving = False
        self._save_buffer = None
        super().__init__(*args, **kwargs)
        Clock.schedule_once(
            self.bind_autosave_listener, .3
        )
        Clock.schedule_interval(self.check_save_buffer, .5)

    @fail_gracefully()
    def check_save_buffer(self, *args, **kwargs):
        if self._save_buffer:
            import threading
            # create a new thread to execute a target coroutine
            thread = threading.Thread(
                target=self.setup_thread,
                args=(self.commit_save_buffer(),)
            )
            # start the new thread
            thread.start()
            # asyncio.create_task(
            #     self.commit_save_buffer()
            # )


    @fail_gracefully()
    def setup_thread(self, coro, *args, **kwargs):
        loop = asyncio.new_event_loop()
        try:
            loop.run_until_complete(coro)            
        finally:
            loop.close()



    # @fail_gracefully()
    @with_db()
    @asyncio_thread()
    async def commit_save_buffer(self, *args, session=None, **kwargs):


        self.change_cache = {}
        self._save_buffer = None


    @with_db()
    async def save(self, *args, session=None, **kwargs):
        if not self.model_class:
            write_to_log(
                f"Cannot Save! Model class not defined on {self.__class__}",
                level='warning'
            )
            return

        if not await self.is_valid(session=session):
            return
            
        instance_kwargs = copy(self.change_cache)
        if hasattr(self, "ai"):
            instance_kwargs['account_id'] = self.ai._account_uid

        obj = self.model_class(**instance_kwargs)
        session.add(obj)
        await session.commit()
        await session.refresh(obj)


    @asyncio_thread()
    @with_db()
    async def async_do_autosave(self, *args, session=None, **kwargs):
        if self.disable_autosave or not self.obj:
            return

        self._saving = True

        if not self.model_class:
            write_to_log(
                f"Cannot Save! Model class not defined on {self.form_class}",
                level='warning'
            )
            return

        if not await self.is_valid(session=session):
            return

        try:
            session.add(self.obj)
        except InvalidRequestError:
            pass

        app = App.get_running_app()
        update_dict = {
            "id":self.obj.id, "tiny_applet_form":self._form
        } | self.change_cache
        await app.db_queue.put(update_dict)
        # await app.ai.update_obj_from_dict(
        #     self.obj, update_dict, tiny_applet_form=self._form, session=session
        # )

        # try:
        #     await session.refresh(self.obj)
        # except InvalidRequestError:
        #     pass

        # self.change_cache = {}
        self._saving = False
        

    def bind_autosave_listener(self, *args, **kwargs):
        self.bind(on_form_changed=self.do_autosave)


    def do_autosave(self, *args, **kwargs):
        import threading
        write_to_log(f"Autosave thread: {threading.get_ident()}")
        asyncio.create_task(self.async_do_autosave())