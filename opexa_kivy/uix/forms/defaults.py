FIELD_CLASS_TO_WIDGET_MAPPING = {
	"DateField": "opexa_kivy.uix.forms.fields.contrib.DateFieldWidget",
	"TextField": "opexa_kivy.uix.forms.fields.contrib.TextFieldWidget",
	"IntegerField": "opexa_kivy.uix.forms.fields.contrib.IntegerFieldWidget",
	"FloatField": "opexa_kivy.uix.forms.fields.contrib.FloatFieldWidget",
	"ChoiceField": "opexa_kivy.uix.forms.fields.contrib.ChoiceFieldWidget",
	"ModelSelectField": "opexa_kivy.uix.forms.fields.contrib.ModelSelectFieldWidget",
	"ImageField": "opexa_kivy.uix.forms.fields.contrib.ImageFieldWidget",
	"BooleanField": "opexa_kivy.uix.forms.fields.contrib.BooleanFieldWidget",
	"FormSetField": "opexa_kivy.uix.forms.fields.contrib.FormSetFieldWidget",
	"FileField": "opexa_kivy.uix.forms.fields.contrib.FileFieldWidget"
}