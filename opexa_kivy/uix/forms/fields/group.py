from copy import copy
import traceback

from kivy.uix.boxlayout import BoxLayout
from kivy.metrics import dp
from kivy.lang import Builder
from kivy.clock import Clock

from opexa_kivy import applet

from opexa_kivy.utils import write_to_log
from .mixins import HideShowMixin




Builder.load_string("""
#: import applet opexa_kivy.applet

<FieldGroupWidget>:
    height: applet.settings.FORM_FIELD_HEIGHT
    size_hint: 1, None

    # canvas.before:
    #     Color:
    #         rgb: 0.5, 0.23, 0.05
            
    #     Rectangle:
    #         pos: root.pos
    #         size: root.size

""")

class FieldGroupWidget(HideShowMixin, BoxLayout):

    name = ''
    spacer_width = dp(15)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    # def on_size(self, *args, **kwargs):
    #     write_to_log(f'Group is {self.size}')
    #     for c in self.children:
    #         write_to_log(c.size)


    def __repr__(self, *args, **kwargs):
        return "Field Group: " + ",".join(map(str, self.children))

    def contains_fields(self, name_list):
        for c in self.children:
            if c.name in name_list:
                return True
        return False
    
    def validate(self):
        is_valid = True
        for element in self.children:
            if hasattr(element, 'validate'):
                if not element.validate():
                    is_valid = False
        return is_valid