from calendar import TextCalendar
from copy import copy
from functools import partial
from pathlib import Path
from uuid import uuid4, UUID
import asyncio
import datetime
import functools
import time
import traceback
from os.path import join
import os
from math import ceil
from decimal import Decimal, getcontext, ROUND_DOWN
from collections.abc import Iterable

from dateutil.relativedelta import relativedelta
from kivy.animation import Animation
from kivy.clock import Clock
from kivy.event import EventDispatcher
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import (ObjectProperty, NumericProperty, BooleanProperty, ColorProperty, StringProperty,
    ListProperty, ColorProperty)
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.scrollview import ScrollView
from natsort import natsorted
from sqlalchemy import select, nullslast
from tiny_applet import settings
from tiny_applet.orm.models import ModelBase
from tiny_applet.orm.decorators import with_db
from tiny_applet.utils import write_to_log, string_to_date, fail_gracefully, TimedCode, import_class
from tiny_applet.forms.utils import _null, TrueNull
from kivy.app import App
from sqlalchemy.types import DECIMAL
from sqlalchemy.orm.exc import DetachedInstanceError


from .decorators import silence_input_event_dispatch
from .mixins import BaseClassInitProtectionMixin, TextBasedAppearanceMixin
from opexa_kivy.fonts import icon
from opexa_kivy.uix.dropdown import DropDown
from opexa_kivy.uix.button import OpexaButton, ClickableLabel
from opexa_kivy.uix.popup import OpexaPopupWidget
from opexa_kivy.uix.recycleview import OpexaRecycleView, OpexaLabelDataView
from opexa_kivy.uix.text_input import FormTextInput as KivyTextInput
from opexa_kivy.utils import get_color_module
from opexa_kivy.uix.label import OpexaLabel
colors = get_color_module()

class FieldWidget(EventDispatcher):
    base_text_input_touch_down = False
    dispatch_user_input_on_text = False
    font_color = ColorProperty([0,0,0,0.5])
    activation_delay = 0.15
    verbose_name = StringProperty("")
    parent_field_widget = ObjectProperty({})
    vertical_padding = NumericProperty(35)
    x_padding = NumericProperty(dp(2))
    is_readonly = BooleanProperty(False)
    bg_color = ColorProperty([1,1,1,0])
    obj = ObjectProperty()
    obj_class = ObjectProperty()
    null_input_values = [None]
    null_verbose_value = "-"
    py_type = None
    field_default = None
    value = ObjectProperty(allownone=True)
    name = StringProperty("")


    def __init__(self, name, field_instance, form, *args, **kwargs):
        self.binding_to_obj = False
        super().__init__(*args, **kwargs)
        self.field_instance = field_instance
        self.verbose_name = self.field_instance.meta.get("verbose_name")
        self.form = form
        self.name = name
        self.value = None 
        self.prev_value = self.default_value
        self.register_event_type('on_user_input')
        self.build()

    @property
    def change_cache_key(self):
        return self.name


    @property
    def sql_column(self):
        if self.obj:
            table = self.obj.__table__
        elif self.obj_class:
            table = self.obj_class.__table__
        else:
            return None

        return getattr(
            table.columns, self.name, None
        )

    @fail_gracefully()
    def build(self, *args, **kwargs):
        pass


    @fail_gracefully()
    def bind_to_obj(self, obj, *args, **kwargs):
        self.binding_to_obj = True
        value = copy(getattr(obj, self.name, None))
        if self.is_null_value(value):
            value = self.default_value

        self.set_value_to(
            value, initializing=True,
            sql_column=self.sql_column
        )
        self.binding_to_obj = False


    def value_to_input_text(self, *args, **kwargs):
        return None


    @fail_gracefully()
    def dispatch_on_user_input(self, value, *args, fake=False, **kwargs):
        """
        Before sending the 'on_user_input' event out, input widget checks to see
        if this is a real user input. For example, we might be setting the initial value of
        the form, not receiving external input.
        """
        if fake:
            return

        should_dispath = not getattr(self, '_silence_next_input_event', False)
        
        if should_dispath:
            self.dispatch(
                'on_user_input', self, value, *args, **kwargs
            )


        return should_dispath
        

    @fail_gracefully()
    def on_user_input(self, widget, value, *args, input_text=None, **kwargs):
        self.set_value_to(
            value,
            input_text=input_text,
            sql_column=self.sql_column
        )

    @property
    def default_value(self, *args, **kwargs):
        ret = self.field_default
        if type(self.sql_column) == type(None):
            return ret

        if self.sql_column.default:
            ret = self.sql_column.default.arg
        return ret


    @fail_gracefully()
    def is_null(self, *args, **kwargs):
        return self.value in self.null_input_values


    @fail_gracefully()
    def is_null_value(self, value, *args, **kwargs):
        return value in self.null_input_values


    @silence_input_event_dispatch()
    def set_value_to(self, value, *args, initializing=False, **kwargs):
        """
        Hook used by classes derived from this base.
        **Should ALWAYS have the  'silence_input_event_dispatch' decorator, to
        prevent this being misinterpreted as user input**
        """

        if self.is_null_value(value):
            value = self.default_value

        self.prev_value = copy(self.value)
        self.value = value

        if not initializing:
            self.form.dispatch(
                "on_form_changed",
                self.field_instance,
                self.value
            )
            self.add_value_to_form_change_cache(self.value)


    def add_value_to_form_change_cache(self, value, *args, **kwargs):
        self.form.change_cache[self.change_cache_key] = value        


    async def set_value_to_async(self, value_coroutine, *args, **kwargs):
        """ We just need to await the coroutine in this method so that the result
        returns. Generally, setting properties safely means using the Clock to schedule
        it to be updated on the next tick. """
        Clock.schedule_once(partial(self.set_value_to, await value_coroutine), 0)
        
    @with_db()
    async def is_valid(self, *args, **kwargs):
        required = self.field_instance.meta.get("required", False)
        if required and self.value in self.null_input_values:
            kwargs['error_dict']["required"].append(self.name) 
            return False
        return True


Builder.load_string("""
<FormSetFieldWidget>:
    size_hint_y: None
    height: self.minimum_height
    orientation: "vertical"
    padding: dp(2), dp(5)
    spacing: dp(5)

    canvas.before:
        Color:
            rgba: colors.dark_blue
            a: .8

        Line:
            rounded_rectangle: self.x, self.y, self.width, self.height, dp(5)

    BoxLayout:
        size_hint_y: None
        height: self.minimum_height
        orientation: "vertical"
        padding: [0, 0, 0, dp(5)]
        spacing: dp(10)

        OpexaLabel:
            text: root.verbose_name + root.count_str
            color: colors.pale_grey

        ClickableLabel:
            text: icon("circle-plus") + " Add " + root.verbose_name
            on_release: root.add_form()
            font_size: settings.FONT_SM
            color: colors.dark_green
            bold: True

    FormSetScrollView:
        id: formset_scrollview


<FormSetHolder>:
    size_hint_y: None
    height: self.minimum_height

    canvas.before:
        Color:
            rgba: colors.pale_grey
            a: .25 if self.is_even else 0
        RoundedRectangle:
            pos: self.pos
            size: self.size
            radius: dp(5),

<FormSetScrollView>:
    size_hint: 1, None

    GridLayout:
        id: formset_holder
        cols:1
        size_hint_y: None
        height: self.minimum_height

""")

class FormSetHolder(BoxLayout):
    is_even = BooleanProperty(True)


class FormSetScrollView(ScrollView):
    max_height = NumericProperty(dp(200))


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        def _f(*args):
            self.ids["formset_holder"].bind(
                minimum_height=self.adjust_scrollview_height
            )
        Clock.schedule_once(_f, 0)

    @fail_gracefully()
    def adjust_scrollview_height(self, formset_holder, height, *args, **kwargs):
        if height > self.max_height:
            self.height = self.max_height
        else:
            self.height = height


class FormSetFieldWidget(FieldWidget, BoxLayout):
    count_str = StringProperty()    

    def __init__(self, *args, **kwargs):
        self.__first_form_rendered = False
        super().__init__(*args, **kwargs)
        self.add_form()


    @fail_gracefully()
    def bind_to_obj(self, obj, *args, **kwargs):
        self.binding_to_obj = True
        self.obj = obj

        def _filter_fn(x):
            x_id = getattr(x, self.field_instance.fk_field_name, None)
            return x_id == obj.id

        model_cache = App.get_running_app().sm.model_cache
        related_objects = list(filter(
            _filter_fn,
            model_cache.get_by_class(self.field_instance.fk_model)
        ))

        for index, obj in enumerate(related_objects):
            if index == 0:
                self._first_form.bind_obj_to_form(obj)
            else:
                self.add_form(obj=obj)

        self.binding_to_obj = False


    @with_db()
    async def is_valid(self, *args, **kwargs):
        if await super().is_valid(*args, **kwargs):
            all_empty = True
            for holder in self.ids["formset_scrollview"].ids["formset_holder"].children:
                if holder.form_widget.change_cache:
                    all_empty = False

            if all_empty:
                return True

            for holder in self.ids["formset_scrollview"].ids["formset_holder"].children:
                ret = await holder.form_widget.is_valid(
                    *args, **kwargs
                )
                if not ret:
                    return ret
        return True


    @fail_gracefully()
    def add_form(self, *args, obj=None, **kwargs):
        from opexa_kivy.uix.forms.widget import FormWidget
        is_even = len(
            self.ids["formset_scrollview"].ids["formset_holder"].children
        ) % 2 == 0

        holder = FormSetHolder(is_even=is_even)
        form_widget_kwargs = {
            "form_class":self.field_instance.form_class,
            "target":self.form.target
        }
        if self.field_instance.fk_model:
            form_widget_kwargs['model_class'] = self.field_instance.fk_model
            
        if obj:
            form_widget_kwargs['obj'] = obj

        form_widget = FormWidget(**form_widget_kwargs)
        form_widget.bind(on_form_changed=self.parse_formset_values)
        holder.add_widget(form_widget)
        holder.form_widget = form_widget

        
        remove_btn_kwargs = {
            "text":icon("circle-minus"),
            "on_release":partial(
                self.remove_form,
                holder=holder
            ),
            "font_size":settings.FONT_MD,
            "color":colors.red,
            "size_hint_x":None,
            "width":dp(20)
        }

        if not self.__first_form_rendered:
            self.__first_form_rendered = True
            remove_btn_kwargs.pop("on_release")
            remove_btn_kwargs["color"] = colors.transparent
            self._first_form = form_widget

        remove_btn = ClickableLabel(
            **remove_btn_kwargs
        )
        holder.add_widget(remove_btn)
        self.ids["formset_scrollview"].ids["formset_holder"].add_widget(holder)
        self.adjust_count_str()

    @fail_gracefully()
    def adjust_count_str(self, *args, **kwargs):
        count = len(self.ids["formset_scrollview"].ids["formset_holder"].children)
        if count > 1:
            self.count_str = f" [b]({count})[/b]"
        else:
            self.count_str = ""


    @fail_gracefully()
    def parse_formset_values(self, *args, **kwargs):
        asyncio.create_task(
            self.async_parse_formset_values(*args, **kwargs)
        )


    @fail_gracefully()
    async def async_parse_formset_values(self, *args, **kwargs):
        parsed_values = []
        for holder in self.ids["formset_scrollview"].ids["formset_holder"].children:
            if await holder.form_widget.is_valid(silence_errors=True):

                # Optionally pass in id for existing objects
                id_dict = {}
                if holder.form_widget.obj:
                    id_dict["id"] = holder.form_widget.obj.id

                if self.obj:
                    kwargs_to_parse = holder.form_widget.change_cache
                else:
                    kwargs_to_parse = holder.form_widget.clean_form_data

                if not kwargs_to_parse:
                    continue

                parsed_values.append(kwargs_to_parse | id_dict)

        self.dispatch_on_user_input(parsed_values)


    @fail_gracefully()
    def remove_form(self, *args, holder=None, **kwargs):
        self.ids["formset_scrollview"].ids["formset_holder"].remove_widget(holder)
        self.remove_widget(holder)
        self.adjust_count_str()


Builder.load_string("""
<TextFieldWidget>:
    unfocus_on_touch: True
    # shorten: True
    font_size: settings.FONT_MD
    markup: True
    input_type: "text"
    keyboard_suggestions: True
    padding: [0, self.height/2 - dp(2), 0, 0]
    font_color: colors.dark_blue

    canvas.before:

        Color:
            rgba: self.underline_color
        Line:
            points: root.x, root.y, root.x+root.width, root.y

        Color:
            rgba: self.font_color

    canvas.after:
        Color:
            rgba: [1, 1, 1, self.label_texture_opacity]
        Rectangle:
            pos: self.x, self.y + dp(2)
            size: self.label_texture_size
            texture: self.label_texture

        Color:
            rgba: [1, 1, 1, self.label_upper_texture_opacity]
        Rectangle:
            pos: self.x, self.top - dp(10)
            size: self.label_upper_texture_size
            texture: self.label_upper_texture


        
""")

class TextFieldWidget(TextBasedAppearanceMixin, FieldWidget, KivyTextInput):
    normal_font_color = ListProperty([0,0,0,1])
    inactive_font_color = ListProperty([0,0,0,0.5])
    update_field_widget_on_text_change = True
    underline_color = ColorProperty(colors.dark_blue)
    label_texture = ObjectProperty()
    label_texture_size = ObjectProperty((10,10))
    label_texture_opacity = NumericProperty(1)

    label_upper_texture = ObjectProperty()
    label_upper_texture_size = ObjectProperty((10,10))
    label_upper_texture_opacity = NumericProperty(0)

    base_text_input_touch_down = True
    field_default = ""
    MAX_ROWS = 2

    def __init__(self, *args, **kwargs):
        self.label_upper_anim = Animation(
            label_upper_texture_opacity=1, duration=.5
        )
        super().__init__(*args, **kwargs)
        Clock.schedule_once(self.render_label, 0.06)
        self.multiline = self.field_instance.meta.get("multiline", False)
        if self.multiline:
            self.do_wrap = True

        self.font_size = self.field_instance.meta.get(
            "font_size", settings.FONT_MD
        )



    @fail_gracefully()
    def render_label(self, *args, **kwargs):
        if self.field_instance.meta.get('required', False):
            txt = copy(self.verbose_name) + "*"
            txt = f"[b]{txt}[/b]"
        else:
            txt = self.verbose_name

        help_text = self.field_instance.meta.get(
            "help_text", None
        )
        if help_text:
            txt += f" [i]{help_text}[/i]"

        l = OpexaLabel(
            text=txt,
            color=colors.pale_grey,
            width=self.width, halign="left"
        )
        l.texture_update()
        self.label_texture_size = l.texture.size
        self.label_texture = l.texture

        l = OpexaLabel(
            text=txt,
            color=colors.pale_grey,
            width=self.width, halign="left",
            font_size=settings.FONT_SM
        )
        l.texture_update()
        self.label_upper_texture_size = l.texture.size
        self.label_upper_texture = l.texture


    @fail_gracefully()
    def on_text(self, instance, value, *args, **kwargs):
        self.update_label_opacity(value)

        if self.__class__ == TextFieldWidget or self.__class__.dispatch_user_input_on_text:
            self.dispatch_on_user_input(value)

        if self.__class__ == TextFieldWidget:
            if self.multiline:
                def _fix_height(*args):
                    if self.cursor_row < self.MAX_ROWS:
                        row_for_height = self.cursor_row
                    else:
                        row_for_height = self.MAX_ROWS
                    self.height = settings.FONT_MD + (1+row_for_height)*(settings.FONT_MD + self.line_spacing)
                    self.font_size = settings.FONT_MD
                Clock.schedule_once(_fix_height, 0)


    @fail_gracefully()
    def on_touch_down(self, touch):
        if not super().on_touch_down(touch) and self.collide_point(*touch.pos):
            if self.__class__ == TextFieldWidget or self.__class__.base_text_input_touch_down:
                return KivyTextInput.on_touch_down(self, touch)
        

    @silence_input_event_dispatch()
    def set_value_to(self, value, *args, input_text=None, **kwargs):
        if input_text != None:
            self.text = input_text
        else:
            self.text = str(value)
        super().set_value_to(
            value, *args, input_text=input_text, **kwargs
        )

    def toggle_animation_busy(self, *args, **kwargs):
        self.busy_animating = not self.busy_animating


    def update_label_opacity(self, current_text):
        if not current_text:
            self.label_texture_opacity = 1
            self.label_upper_anim.stop(self)
            self.label_upper_texture_opacity = 0
            
        else:
            self.label_texture_opacity = 0
            self.label_upper_anim.start(self)

Builder.load_string("""
#: import FileChooserListView kivy.uix.filechooser.FileChooserListView
<FileFieldWidget>:
    orientation: "vertical"

    OpexaLabel:
        text: root.verbose_name
        color: colors.pale_grey
        bold: True

    ClickableLabel:
        text: root.verbose_value if root.value else "(tap to upload)"
        color: colors.dark_blue if root.value else colors.dark_green
        on_release: root.show_filechooser()
        shorten: True
        maxlines: 2

<FileLoadDialogue>:
    canvas.before:
        Color:
            rgba: 0,0,0,.6
        Rectangle:
            size: self.size
            pos: self.pos

    BoxLayout:
        size_hint_y:None
        height: Window.height*.7
        orientation: "vertical"

        FileChooserIconView:
            id: filechooser

        BoxLayout:
            size_hint_y: None
            height: 30
""")

class FileFieldWidget(FieldWidget, BoxLayout):
    field_default = ""
    verbose_value = StringProperty("-")

    @fail_gracefully()
    def on_value(self, *args, **kwargs):
        if self.value:
            self.verbose_value = self.value.split("/")[-1]



    @fail_gracefully()
    def show_filechooser(self, *args, **kwargs):
        from kivy.app import App
        from kivy.utils import platform

        if platform == 'android':
            from jnius import autoclass, cast
            Intent = autoclass("android.content.Intent")
            currentActivity = cast(
                'zw.co.rabbitmanagement.RMSActivity',
                autoclass('zw.co.rabbitmanagement.RMSActivity').mActivity
            )
            intent = Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setType("*/*")        
            currentActivity.startActivityForResult(
                intent, currentActivity.CHOOSE_FILE_REQUEST_CODE
            )
            Clock.schedule_once(
                partial(self.retrieve_file_from_activity, currentActivity),
                .1
            )

        else:
            popup = FileLoadDialogue(target=self)
            popup.ids["filechooser"].path = str(Path.home().resolve())
            popup.open()


    @fail_gracefully()
    def retrieve_file_from_activity(self, currentActivity, *args, **kwargs):

        if not currentActivity.mimeType:

            Clock.schedule_once(
                partial(
                    self.retrieve_file_from_activity, currentActivity
                ), .1
            )
            return

        from jnius import autoclass, cast

        upload_path = Path(settings.BASE_DIR, "file_uploads")
        upload_path.mkdir(parents=True, exist_ok=True)

        filename_str = currentActivity.fileName.split("/")[-1] + "." + currentActivity.mimeType
        relative_path = f"file_uploads/{filename_str}"
        new_file_p = Path(upload_path, filename_str)
        new_file_p.unlink(missing_ok=True)
        new_file_p.touch(exist_ok=True)
        with new_file_p.open("w") as write_file:
            write_file.write(currentActivity.fileContentStr)

        Boolean = autoclass('java.lang.Boolean')
        String = autoclass('java.lang.String')
        currentActivity.fileContentStr = String()
        currentActivity.fileName = String()
        currentActivity.mimeType = String()
        self.dispatch_on_user_input(relative_path)


    def save_file(self):
        with Path(target_file).open("rb") as read_file:
            filename = target_file.split("/")[-1]
            upload_path = Path(self.upload_dir, filename)
            with upload_path.open("wb") as write_file:
                write_file.write(
                    read_file.read()
                )
        write_to_log(selection, *args, **kwargs)


class FileLoadDialogue(OpexaPopupWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        def _bind_listeners(*args):
            self.ids["filechooser"].bind(
                on_submit=self.handle_file_selection
        )
        Clock.schedule_once(_bind_listeners, 0)

    @property
    def upload_dir(self):
        return Path(settings.BASE_DIR, "uploads")

    @fail_gracefully()
    def handle_file_selection(self, widget, file_list, *args, **kwargs):
        self.target.dispatch_on_user_input(file_list[0])
        self.dismiss()
        

Builder.load_string("""
<ImageFieldWidget>:
    orientation: "vertical"

    OpexaButton:
        id: button
        text: "Image"
        size_hint: (0.4, None)
        pos_hint: {'center_x':0.5}
        on_release: root.capture()

    Camera:
        id: camera
        play: False

<BooleanFieldWidget>:
    
    orientation: "vertical"
    spacing: dp(5)

    OpexaLabel:
        text: root.verbose_name
        color: colors.pale_grey
        font_size: settings.FONT_SM

    ClickableLabel:

        canvas.before:
            Color:
                rgba: colors.dark_blue
            SmoothEllipse:
                pos: self.pos[0] + self.width/2 - self.height/2, self.pos[1]
                size: self.height, self.height
                # radius: [dp(5),]

            Color:
                rgba: colors.white
            SmoothEllipse:
                pos: self.pos[0] + self.width/2 - self.height/2 + dp(1), self.pos[1] + dp(1)
                size: self.height-dp(2), self.height - dp(2)
                # radius: [dp(5),]

            Color:
                rgba: root.checkbox_color
            SmoothEllipse:
                pos: self.pos[0] + self.width/2 - self.height/2 + dp(3), self.pos[1] + dp(3)
                size: self.height - dp(6), self.height - dp(6)

        id: checkbox
        text: ' '
        font_size: settings.FONT_MD
        pos_hint: {'center_x':.5, 'center_y':.5}
        on_release: root.toggle_value()
        default_animation: False
""")


class ImageFieldWidget(FieldWidget, BoxLayout):

    @property
    def in_capture_mode(self):
        return self.ids['button'].text == "Capture"

    def capture(self, *args, **kwargs):

        camera = self.ids['camera']
        button = self.ids['button']
        if self.in_capture_mode:
            filename = join(
                settings.BASE_DIR, self.field_instance.upload_dir, str(uuid4())
            ) + ".png"    
            camera.texture.save(filename)

            camera.play = False
            button.text = "Image"
            camera.opacity = 0
            self.set_value_to(filename)

        else:
            camera.play = True
            button.text = "Capture"
            camera.opacity = 1
            self.set_value_to('')



class BooleanFieldWidget(FieldWidget, BoxLayout):
    text = StringProperty("")
    checkbox_color = ColorProperty([0,0,0,0])
    fade_in_anim = Animation(checkbox_color=colors.chart_blue, duration=.3)
    fade_out_anim = Animation(checkbox_color=[0,0,0,0], duration=.1)
    field_default = False


    def toggle_value(self, *args, **kwargs):
        self.dispatch_on_user_input(not self.value)


    def set_value_to(self, *args, **kwargs):
        super().set_value_to(*args, **kwargs)

        if self.value:
            self.fade_out_anim.stop(self)
            self.fade_in_anim.start(self)
        else:
            self.fade_in_anim.stop(self)
            self.fade_out_anim.start(self)


Builder.load_string("""
<IntegerFieldWidget>:
    canvas.after:
        Color:
            rgba: [1, 1, 1, self.increment_btn_texture_opacity]
        Rectangle:
            pos: self.x + self.width - self.increment_btn_texture_size[0], self.y + self.height/2
            size: self.increment_btn_texture_size
            texture: self.increment_btn_texture

        Color:
            rgba: [1, 1, 1, self.decrement_btn_texture_opacity]
        Rectangle:
            pos: self.x + self.width - self.decrement_btn_texture_size[0], self.y
            size: self.decrement_btn_texture_size
            texture: self.decrement_btn_texture
""")

class IntegerFieldWidget(TextFieldWidget):
    dispatch_user_input_on_text = True
    increment_btn_texture = ObjectProperty()
    increment_btn_texture_size = ObjectProperty((10,10))
    increment_btn_texture_opacity = NumericProperty(1)
    decrement_btn_texture = ObjectProperty()
    decrement_btn_texture_size = ObjectProperty((10,10))
    decrement_btn_texture_opacity = NumericProperty(1)
    py_type = int
    field_default = 0


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.input_filter = "int"
        self.input_type = "number"
        self.keyboard_suggestions = False
        self._recovery_time = datetime.datetime.now()
        Clock.schedule_once(
            self.render_controls, .04
        )


    @fail_gracefully()
    def on_touch_down(self, touch):
        right = self.pos[0] + self.width + dp(10)
        left = right - self.increment_btn_texture_size[0] - dp(10)
        top = self.pos[1] + self.height + dp(10)
        bottom = self.pos[1] - dp(10)
        v_middle = bottom + (top-bottom)/2
        h_inside = touch.pos[0]>=left and touch.pos[0]<=right
        v_inside = touch.pos[1]>=bottom and touch.pos[1]<=top

        if h_inside and v_inside:
            if touch.pos[1] >= v_middle:
                self.increment_value()
            else:
                self.decrement_value()
            self.focus = False
            return True
        else:
            return super().on_touch_down(touch)

    @fail_gracefully()
    def animate_press(self, action, *args, **kwargs):
        Animation.cancel_all(self)
        if action == "increase":
            opacity_attr = "increment_btn_texture_opacity"
        else:
            opacity_attr = "decrement_btn_texture_opacity"
        opacity_var = setattr(self, opacity_attr, .3)
        write_to_log(action, {opacity_attr:1})
        anim = Animation(duration=.5, **{opacity_attr:1})
        anim.start(self)


    @fail_gracefully()
    def _update_recovery_time(self, *args, **kwargs):
        self._recovery_time = datetime.datetime.now() + relativedelta(seconds=.4)


    @fail_gracefully()
    def has_recovered(self, *args, **kwargs):
        return datetime.datetime.now() > self._recovery_time


    @fail_gracefully()
    def increment_value(self, *args, **kwargs):
        if not self.has_recovered():
            return

        self.animate_press("increase")
        if self.value:
            new_value = self.value+1
        else:
            new_value = 1
        self.set_value_to(new_value)
        self._update_recovery_time()



    @fail_gracefully()
    def decrement_value(self, *args, **kwargs):
        if not self.has_recovered():
            return

        self.animate_press("decrease")
        if self.value:
            new_value = self.value-1
        else:
            new_value = -1
        self.set_value_to(new_value)
        self._update_recovery_time()


    @fail_gracefully()
    def render_controls(self, *args, **kwargs):
        l = OpexaLabel(
            text=icon("chevron-up"),
            color=colors.dark_blue,
            width=dp(35),
            size_hint_y=None,
            height= self.height/2
        )
        l.texture_update()
        self.increment_btn_texture_size = l.texture.size
        self.increment_btn_texture = l.texture

        l = OpexaLabel(
            text=icon("chevron-down"),
            color=colors.dark_blue,
            width=dp(35),
            size_hint_y=None,
            height= self.height/2
        )
        l.texture_update()
        self.decrement_btn_texture_size = l.texture.size
        self.decrement_btn_texture = l.texture
    

    @silence_input_event_dispatch()
    @fail_gracefully()
    def set_value_to(self, value, *args, sql_column=None, **kwargs):
        """ Convert the fields current value to a human readable string. """
        # May be a value we already know represents null
        input_text = None

        if value in self.null_input_values:
            self.text = self.null_verbose_value
            return

        if value == "" or value == 0:
            value = 0
            input_text = ""

        else:

            input_text = str(value) # Makes sure zeros after decimal appear
            if input_text == ".": # Always put 0 in front of decimal
                self.text = "0."
                def _f(*args):
                    self.do_cursor_movement("cursor_end")
                Clock.schedule_once(_f, 0)
                return   

            elif input_text.endswith('.'): # Mean's user trying to input a float
                return

            # Represent value internally as the correct python type
            if "." in input_text:
                if sql_column.type.__class__ == DECIMAL:
                    quantize_to = str(1/10**sql_column.type.precision)
                    decimal_value = Decimal(value).quantize(
                        Decimal(quantize_to), rounding=ROUND_DOWN
                    )
                    original_text = copy(input_text)
                    input_text = str(decimal_value)
                    if original_text.endswith(f"0"):
                        digits, decimals = original_text.split(".")
                        input_text = digits + "." + decimals[:sql_column.type.precision]

                    else:
                        while input_text[-1] == "0":
                            input_text = input_text[:-1]                   

                    value = decimal_value
                    
                    
                else:
                    value = float(value)
            
            else:
                value = int(value)

        kwargs["input_text"] = input_text
        super().set_value_to(value, *args, **kwargs)



class FloatFieldWidget(IntegerFieldWidget):
    py_type = float

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.input_filter = "float"



Builder.load_string("""
#: import OpexaLabel opexa_kivy.uix.label.OpexaLabel

<SelectInput>:
    orientation: "horizontal"

<SelectOption>:
    OpexaLabel:
        text: root.label

    CheckBox:
        id: checkbox
        group: root.group

<BooleanInput>:
    orientation: "vertical"

    OpexaLabel:
        size_hint: (1, None)
        text: root.parent_field_widget.get_hint_text()
        halign: "center"
        font_size: settings.FONT_SM

    CheckBox:
        id: checkbox
        size_hint: (None, 1)
        active: root.parent_field_widget.value
        width: dp(30)
        pos_hint: {'center_x':.5}        


<MultipleChoiceRecycleview>
    canvas.before:
        Color:
            rgb: [.67, .9, .1]
            a: 0.4
        RoundedRectangle:
            pos: self.pos
            size: self.size

<MultipleModelSelectInput>:
    BoxLayout:
        orientation: 'vertical'
        padding: [dp(5),]
        pos_hint: {'center_y':0.5, 'center_x':0.5}

        BoxLayout:
            orientation: 'horizontal'
            padding: [dp(10), ]
            spacing: dp(5)
            id: master_container

            BoxLayout:
                orientation: 'vertical'
                id: unselected_container

                OpexaLabel:
                    text: "Available"
                    size_hint: 1, None
                    height: dp(20)
                    font_size: dp(10)

                MultipleChoiceRecycleview:
                    id: unselected
                    viewclass: "OpexaLabelDataView"

            BoxLayout:
                orientation: 'vertical'

                OpexaLabel:
                    text: "Selected"
                    size_hint: 1, None
                    height: dp(20)
                    font_size: dp(10)

                MultipleChoiceRecycleview:
                    id: selected
                    viewclass: "OpexaLabelDataView"
""")

class AddOption():
    pass

class NullOption():
    id = None

class SelectOption(BoxLayout):
    label = StringProperty("")
    value = StringProperty("")
    group = StringProperty("")

    def set_active(self):
        self.ids['checkbox'].active = True


class ChoiceFieldWidget(TextFieldWidget):

    options_cache = ListProperty([])
    can_add = False


    def __init__(self, *args, **kwargs):
        self.__inhibit_search = False
        self.dropdown = None
        super().__init__(*args, **kwargs)
        asyncio.create_task(self.pre_cache_options())

    @property
    def default_value(self, *args, **kwargs):
        return None


    @with_db()
    async def pre_cache_options(self, *args, session=None, bind_to_obj=True, **kwargs):
        self.options_cache = self.create_dropdown_options(
            await self.get_choices(session=session)
        )

        if bind_to_obj:
            if self.obj:
                self.bind_to_obj(self.obj)


    def __perform_search(self, *args, **kwargs):
        
        if not self.dropdown or self.__inhibit_search:
            return

        results = []
        for opt in self.options_cache:
            if self.text.lower() in opt[0].lower():
                results.append(opt)
        self.dropdown.options = results

        if not results:
            self.dropdown.rv.show_no_results()
        else:
            self.dropdown.rv.hide_no_results()

    @fail_gracefully()
    def create_dropdown_options(self, choice_iter, *args, **kwargs):
        option_tuple_list = []
        for choice_tuple in choice_iter:
            opt_tuple = (
                choice_tuple[1],
                partial(
                    self.on_selection, choice_tuple[0],
                    input_text=choice_tuple[1]
                ),
                choice_tuple[0]
            )
            option_tuple_list.append(opt_tuple)
        return option_tuple_list


    def convert_cached_to_value(self, cached_value, *args, **kwargs):
        return cached_value


    def convert_cached_to_input_text(self, cached_value, *args, raw_cache=None, **kwargs):
        if not raw_cache:
            write_to_log("Cache not provided", level="warning")
            return
        return raw_cache[0]

    @fail_gracefully()
    def bind_to_obj(self, obj, *args, **kwargs):
        
        # If the options cache hasn't yet been retrieved, do nothing.
        # Should be called again once cache is populated 'on_options_cache'
        if not self.options_cache:
            return

        raw_value = None
        try:
            raw_value = getattr(obj, self.name, None)

        # Field probably refers to a ForeignKey relationship.
        # Retrieve it from the model cache instead.
        except DetachedInstanceError:

            model_cache = App.get_running_app().sm.model_cache
            id_field_name = self.name + "_id"
            obj_id = getattr(obj, id_field_name, None)
            if obj_id:
                try:
                    raw_value = model_cache[obj_id]
                except KeyError:
                    raise RuntimeError(
                        f"Failed to find {self.name} field object in cache with uid {obj_id}")
            else:
                raise RuntimeError(
                    "Can't find object in model cache because field name cannot "
                    "be correctly converted into name of the id_field."
                )

        if raw_value:
            for cache_tuple in self.options_cache:
                if self.item_in_cache(cache_tuple, raw_value):

                    # Found a match
                    self.set_value_to(
                        self.convert_cached_to_value(
                            cache_tuple[2]
                        ),
                        input_text = self.convert_cached_to_input_text(
                            cache_tuple[2], raw_cache=cache_tuple
                        ),
                        initializing=True
                    )
                    break


    def item_in_cache(self, cache_tuple, raw_value, *args, **kwargs):
        return cache_tuple[2] == raw_value


    @with_db()
    async def get_choices(self, *args, session=None, **kwargs):
        return await self.field_instance.get_options(session=session)


    @fail_gracefully()
    def on_selection(self, selected, *args, input_text=None, **kwargs):

        if isinstance(selected, NullOption):
            input_text = ""

        selected_value = self.convert_cached_to_value(
            selected
        )
        self.dispatch_on_user_input(
            selected_value, input_text=input_text
        )


    @fail_gracefully()
    def on_text(self, *args, **kwargs):
        super().on_text(*args, **kwargs)
        self.__perform_search()
        

    @fail_gracefully()
    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            if self.can_add:
                right = self.pos[0] + self.width
                left = right - self.add_btn_texture_size[0] - dp(10)
                top = self.pos[1] + self.height + dp(20)
                bottom = self.pos[1]

                h_inside = touch.pos[0]>=left and touch.pos[0]<=right
                v_inside = touch.pos[1]>=bottom and touch.pos[1]<=top

                if h_inside and v_inside:
                    Clock.schedule_once(self.show_add_popup, 0)
                    return True

        if not super().on_touch_down(touch):
            if self.collide_point(*touch.pos):
                asyncio.create_task(
                    self.show_selector_dropdown(
                        touch.pos
                    )
                )
                return True


    @with_db()
    async def show_selector_dropdown(self, pos, *args, session=None, **kwargs):
        self.dropdown = DropDown(
            post_dismiss_delay=.1,
            post_dismiss=self.clean_invalid_input
        )
        self.dropdown.options = self.options_cache
        self.dropdown.open(self.to_window(*pos))

        def _f(*args):
            self.select_all()
        Clock.schedule_once(_f, .2)


    @fail_gracefully()
    def clean_invalid_input(self, *args, **kwargs):
        input_found_in_cache = False
        for opt in self.options_cache:
            if self.text == opt[0]:
                input_found_in_cache = True

        if not input_found_in_cache:
            value_to_use = self.value or self.prev_value
            if value_to_use:
                input_text = ""
                for opt in self.options_cache:
                    if opt[2] == value_to_use:
                        input_text=opt[0]

                self.set_value_to(
                    value_to_use, input_text=input_text
                )


    @fail_gracefully()
    def set_value_to(self, value, *args, retry=True, **kwargs):
        if not self.options_cache:
            def _retry(*args):
                self.set_value_to(value, *args, retry=False, **kwargs)

            if retry:
                Clock.schedule_once(_retry, 0)
            return

        dropdown_widget = getattr(self, "dropdown", None)
        if dropdown_widget:
            dropdown_widget.opacity = 0

        if isinstance(value, UUID):
            sm = App.get_running_app().sm
            obj = sm.model_cache.get(value, None)
            if not obj:
                write_to_log(
                    f"Error occured finding {value} in model cache!"
                )
            kwargs['input_text'] = str(obj)

        for opt in self.options_cache:
            if value == opt[0]:
                kwargs['input_text'] = opt[2]

        if value in self.null_input_values:
            value = None
            kwargs['input_text'] = ""


        super().set_value_to(value, *args, **kwargs)
        self.do_cursor_movement("cursor_home")
        
Builder.load_string("""
<ModelSelectFieldWidget>:

    canvas.after:
        Color:
            rgba: [1, 1, 1, self.add_btn_texture_opacity]
        Rectangle:
            pos: self.x + self.width - self.add_btn_texture_size[0], self.y + self.add_btn_texture_size[0]/6
            size: self.add_btn_texture_size
            texture: self.add_btn_texture
""")

class ModelSelectFieldWidget(ChoiceFieldWidget):

    add_btn_texture = ObjectProperty()
    add_btn_texture_size = ObjectProperty((10,10))
    add_btn_texture_opacity = NumericProperty(1)
    can_add = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        Clock.schedule_once(self._render_add_btn, 0)

    
    @property
    def _base_query(self):
        where_clause_args = self.field_instance.meta.get("where_clause_args", [])
        return select(self.field_instance.model_cls).where(*where_clause_args)


    @fail_gracefully()
    def _render_add_btn(self, *args, **kwargs):
        l = OpexaLabel(
            text=icon("circle-plus"),
            color=colors.dark_blue,
            width=dp(25),
            font_size=settings.FONT_LG
        )
        l.texture_update()
        self.add_btn_texture_size = l.texture.size
        self.add_btn_texture = l.texture

    @fail_gracefully()
    def add_value_to_form_change_cache(self, value, *args, **kwargs):
        if hasattr(value, "id"):
            value = value.id
        super().add_value_to_form_change_cache(value, *args, **kwargs)


    @property
    def change_cache_key(self):
        return self.name + "_id"
        

    @fail_gracefully()
    def convert_cached_to_value(self, cached_value, *args, **kwargs):
        return cached_value.id


    @fail_gracefully()
    def convert_cached_to_input_text(self, cached_value, *args, **kwargs):
        return str(cached_value)


    @with_db()
    async def get_choices(self, *args, session=None, **kwargs):

        choices = await self.field_instance.get_options(session=session)
        if choices:
            return choices

        for obj in await self.get_queryset(session=session):
            choice_tuple = (obj, str(obj))
            choices.append(choice_tuple)
            
        if not self.field_instance.meta.get("required"):
            new_choices = [(NullOption(), "None")]
            new_choices.extend(choices)
            choices = new_choices
        return choices

    
    @with_db()
    async def get_queryset(self, *args, session=None, **kwargs) -> list:

        sm = App.get_running_app().sm
        obj_qs = sm.model_cache.get_by_class(
            self.field_instance.model_cls
        )
        ordering_fn = self.field_instance.meta.get(
            "ordering_fn", None
        )
        if ordering_fn:
            if asyncio.iscoroutinefunction(ordering_fn):
                return await ordering_fn(obj_qs)
            else:
                return ordering_fn(obj_qs)

        return natsorted(
            obj_qs, key=lambda x: str(x).lower()
        )


    def item_in_cache(self, cache_tuple, raw_value, *args, **kwargs):
        if not hasattr(raw_value, "id") or not hasattr(cache_tuple[2], "id"):
            return False
        return cache_tuple[2].id == raw_value.id


    @fail_gracefully()
    def show_add_popup(self, *args, **kwargs):
        
        Animation.cancel_all(self)
        self.add_btn_texture_opacity = .3
        anim = Animation(add_btn_texture_opacity=1, duration=.3)
        anim.start(self)
        self.focus = False

        FormClassHandler = import_class("rms_kivy.forms.tools.FormClassHandler")
        popup = FormClassHandler().get_create_popup(
            self.field_instance.model_cls, field_widget=self
        )
        popup.open()

Builder.load_string('''
#: import icon opexa_kivy.fonts.icon
#: import Calendar opexa_kivy.uix.calendar.Calendar

<DateSelectPopup>:
    orientation: "vertical"

    Calendar:
        id: calendar_widget

    AcceptButton:
        pos_hint: {'center_x':0.5}
        popup: root

''')


class DateFieldWidget(TextFieldWidget):
    update_field_widget_on_text_change = False
    date_format = "%d-%m-%y"

    @silence_input_event_dispatch()
    def set_value_to(self, value, *args, **kwargs):
        """ Convert the fields current value to a human readable string. """

        self.current_date = value
        if value not in self.null_input_values:
            if isinstance(value, str):
                self.text = value
            else:
                self.text = datetime.datetime.strftime(value, self.date_format)
            input_text = self.text

        else:
            input_text = ""

        kwargs["input_text"] = input_text
        super().set_value_to(value, *args, **kwargs)

    
    @fail_gracefully()
    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            if self.text:
                current_date = string_to_date(self.text)
            else:
                current_date = getattr(self, "current_date", None) or datetime.date.today()

            popup = DateSelectPopup(current_date)
            popup.bind(on_selection_made=self.update_value)
            popup.open(touch.pos)

            return True


    def update_value(self, instance, value):
        self.set_value_to(value)
        self.dispatch_on_user_input(value)

                
class DateSelectPopup(OpexaPopupWidget):

    def __init__(self, current_date, *args, **kwargs):
        self.submit_fn = self.done
        self.dropdown_register = {}
        if isinstance(current_date, str):
            current_date = string_to_date(current_date)

        super().__init__(*args, **kwargs)
        self.ids["calendar_widget"].displayed_date = current_date
        self.register_event_type("on_selection_made")




    def done(self, *args, **kwargs):
        self.dispatch(
            "on_selection_made", self.ids["calendar_widget"].selected_date
        )


    def on_selection_made(self, *args, **kwargs):
        pass