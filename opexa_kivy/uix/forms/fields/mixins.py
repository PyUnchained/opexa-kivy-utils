from copy import copy

from kivy.uix.button import Button
from tiny_applet.utils import write_to_log

class HideShowMixin():

    def __init__(self, *args, **kwargs):
        self.visible = True
        super().__init__(*args, **kwargs)

    def hide(self, *args):
        def recursive_hide(root_widget):
            root_widget.opacity = 0
            root_widget._prev_size = copy(root_widget.size)
            root_widget.size = (0,0)
            root_widget.visible = False

            for c in root_widget.children:
                recursive_hide(c)
                
        recursive_hide(self)

    def show(self, *args):
        def recursive_show(root_widget):
            
            prev_size = getattr(root_widget, "_prev_size", None)
            if prev_size:
                root_widget.opacity = 1
                root_widget.size = prev_size
                root_widget._prev_size = None
            root_widget.visible = True

            for c in root_widget.children:
                recursive_show(c)

        recursive_show(self)

class BaseClassInitProtectionMixin():
    """ Helper mixin designed to prevent and ancestor classes from having their
    init methods called with arguments, since most of the base Kivy widgets have
    this behavior """

    def __init__(self, *args, **kwargs):
        super().__init__()

class TextBasedAppearanceMixin():

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def on_height(self, instance, height, *args, **kwargs):
        self.font_size = height/2

    def on_input_init(self, *args, **kwargs):
        """ Make sure hint text/initial text is correctly displayed. """
        self.hint_text = self.field_instance.meta.get("verbose_name")

        # Certain classes, for example those descended from the Button class
        # don't have the option of setting a text hint, so instead we have to set the
        # text attribute. Anything descending from TextInput should explicitly avoid
        # this.
        if self.form.obj:
            from opexa_kivy.uix.forms.fields.contrib import TextFieldWidget
            if not (TextFieldWidget in self.__class__.__bases__ or self.__class__ == TextFieldWidget):
                self.text = self.field_instance.meta.get("verbose_name")

        has_value = self.text != self.hint_text and self.text != ''
        if has_value:
            self.font_color = [1,1,1]
            self.label_opacity = 1
            
        super().on_input_init(*args, **kwargs)
