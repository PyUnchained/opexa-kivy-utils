from functools import wraps

class silence_input_event_dispatch():

    def __init__(self, *args):
        pass

    def __call__(self, fn):

        @wraps(fn)
        def silenced_wrapper(instance, *args, **kwargs):
            setattr(instance, '_silence_next_input_event', True)
            resp =  fn(instance, *args, **kwargs)
            setattr(instance, '_silence_next_input_event', False)
            return resp

        return silenced_wrapper