from copy import copy
import traceback

from kivy.uix.boxlayout import BoxLayout
from kivy.metrics import dp
from kivy.lang import Builder
from kivy.clock import Clock

from opexa_kivy import applet
from opexa_kivy.utils import write_to_log
from .mixins import HideShowMixin


Builder.load_string("""
<FieldGroupWidget>:
    height: settings.FORM_FIELD_HEIGHT
    size_hint: 1, None
""")

class FieldGroupWidget(HideShowMixin, BoxLayout):

    name = ''
    spacer_width = dp(15)

    def __repr__(self, *args, **kwargs):
        return "Field Group: " + ",".join(map(str, self.children))

    def contains_fields(self, name_list):
        for c in self.children:
            if c.name in name_list:
                return True
        return False
    
    def validate(self):
        is_valid = True
        for element in self.children:
            if hasattr(element, 'validate'):
                if not element.validate():
                    is_valid = False
        return is_valid