import datetime
from inspect import isclass
from collections.abc import Iterable

from kivy.metrics import dp
from kivy.clock import Clock
from opexa_kivy.uix.forms.fields.contrib import ChoiceFieldWidget
from sqlalchemy import Column, String, Date, Boolean, Integer, Float, DateTime, ForeignKey
from sqlalchemy import inspect
from tiny_applet import settings
from tiny_applet.forms.utils import _null
from tiny_applet.utils import write_to_log, import_class



def field_to_widget(name, field_instance, form, *args, obj=None,
	initial={}, **field_kwargs):
	""" Create the UIX representation of the given form field instance. """

	try:
		WidgetClass = import_class(
			settings.FIELD_CLASS_TO_WIDGET_MAPPING.get(
				field_instance.__class__.__name__
		))

		# Determine human-readable name for field
		if not field_instance.meta['verbose_name']:
			field_instance.meta["verbose_name"] = name.replace("_", " ").title()

		field_kwargs['obj_class'] = getattr(form, "obj_class", None)
		field_widget = WidgetClass(
			name, field_instance, form, **field_kwargs
		)
		field_instance.widget = field_widget
		field_widget.size_hint = field_instance.meta.get("size_hint", (1, None))

		# Adjust the height being set.
		height = field_instance.meta.get(
			"height",
			getattr(field_widget, "default_height", None) or settings.FORM_FIELD_HEIGHT
		)
		field_widget.height = height


		# Associate the field with the correct object/initial data
		# (if any)
		if name in initial or field_instance.meta.get("initial", None):
			value = initial.get(field_widget.name, _null)
			if value == _null:
				value = field_instance.meta.get("initial", _null)

			if value != _null:
					
				def __set_initial_value(*args, **kwargs):
					"""
					Needs to happen on the next frame, to allow
					time for options cache to be populated.
					"""
					set_value_kwargs = {
						"initializing":True
					}
					if WidgetClass == ChoiceFieldWidget:
						for opt in field_widget.options_cache:
							if opt[2] == value:
								set_value_kwargs['input_text'] = opt[0]

					field_widget.set_value_to(
						value, **set_value_kwargs
					)
				Clock.schedule_once(__set_initial_value, 0)

		
		elif obj:
			field_widget.obj = obj
			def __bind_to_obj(*args):
				field_widget.bind_to_obj(obj)
			Clock.schedule_once(__bind_to_obj, 0)
			
		return field_widget

	except:
		write_to_log(
			f'Failed to create field "{name}"  ({field_instance.__class__.__name__})',
			level='error'
		)

def get_default_layout(mapper, field_layout):

	if mapper:
		layout = []
		for orm_descriptor in mapper.all_orm_descriptors:
			name = getattr(orm_descriptor, 'name', None)
			if name and name != 'id':
				layout.append(name)
	else:
		layout = field_layout

	return layout

def get_form_class(field_layout, *args, field_instance_kwargs={},
	field_class_overrides={}, model_class=None, fields=[],  **kwargs):

	""" Returns a generic KivyModelFormWidget for the specified model class.
	Allows customization of the form returned. """

	if model_class:
		mapper = inspect(model_class)
	else:
		mapper = None

	if field_layout:
		flat_layout = flatten_layout(field_layout)
	else:
		flat_layout = get_default_layout(mapper, field_layout)

	# These will become the attributes of the KivyForm class
	FORM_CLASS_DICT = {"layout":field_layout, "model":model_class, "fields":{}}
	
	layout_descriptors = []
	if mapper:

		# Get all the ORM descriptors for db_columns, based on layout
		for orm_descriptor in mapper.all_orm_descriptors:
			name = getattr(orm_descriptor, 'name', None)
			if not name:
				continue

			if name in flat_layout:
				layout_descriptors.append(orm_descriptor)

		# Get all the ORM descriptors for relationship fields
		for name, orm_descriptor in mapper.relationships.items():
			if name in flat_layout:
				orm_descriptor.name = name # So it behaves more like the other descriptors
				layout_descriptors.append(orm_descriptor)

	# Add any explicitly defined fields to the class creation dict
	for f in fields:
		FORM_CLASS_DICT['fields'][f.name] = f

	# Create instances of "rms_common.forms.fields.base.FormField" based on
	# the ORM mapper columns defined
	for orm_descriptor in layout_descriptors:

		# Best case, it's explicitly defined in the Model definition
		form_field_class = orm_descriptor.info.get('form_field_class')

		# Try to guess using the python_type of the column
		if not form_field_class:

			if not getattr(orm_descriptor, 'type', None):
				write_to_log("Warning: The model definition for the "
					f"'{orm_descriptor.name}' field on '{model_class}' "
					"should probably explicitly define 'form_field_class'",
					level='warning')
				continue

			form_field_class = FIELD_MAPPING.get(orm_descriptor.type.python_type)

		# If we've gotten to this point with no field class, something has gone wrong.
		if not form_field_class:
			write_to_log('Warning, failed to find a field class for '
				f'{orm_descriptor.name}.\n'
				f'SQLAlchemy type: {orm_descriptor.type}. '
				f'Python type: {orm_descriptor.type.python_type}.\n',
				level='warning')
			continue

		# Create the form field instances
		field_cls_kwargs = orm_descriptor.info.get('form_field_kwargs') or {}
		field_cls_kwargs = field_cls_kwargs | (
			field_instance_kwargs.get(orm_descriptor.name) or {})
		field_cls_kwargs['name'] = orm_descriptor.name
		field_cls_kwargs['model_class'] = model_class

		form_field_descriptor = form_field_class(**field_cls_kwargs)
		FORM_CLASS_DICT['fields'][orm_descriptor.name] = form_field_descriptor

	if model_class:
		cls_prefix = model_class.__tablename__.title()
	else:
		cls_prefix = 'Generic'

	class_name = f"{cls_prefix}FormWidget"
	base_form_class = import_class("opexa_kivy.uix.forms.widget.FormWidget")
	return type(class_name, (base_form_class, ), FORM_CLASS_DICT)

def flatten_layout(field_layout):
	""" Replaces tuples and returns all field names in a single list. """

	flattned = []
	for element in field_layout:
		if isinstance(element, Iterable) and not isinstance(element, str):
			flattned.extend(element)
		else:
			flattned.append(element)
	return flattned