from functools import partial
import math

from kivy.graphics import *
from kivy.clock import Clock
from kivy.animation import Animation
from kivy.utils import get_hex_from_color
from kivy.metrics import dp
from tiny_applet.utils import write_to_log, fail_gracefully
from tiny_applet import settings

from opexa_kivy.uix.label import OpexaLabel
from opexa_kivy.utils import get_color_module
from opexa_kivy.fonts import icon
from .base import BaseGraph, HorizontalBackgroundMixin

colors = get_color_module()

class LineGraphWidget(HorizontalBackgroundMixin, BaseGraph):

    @fail_gracefully()
    def render_point_graphic(self, *args, width=None, x = None, **kargs):
        point_size =25
        return Ellipse(
            pos=[
                self.drawable_left + x + (width/2) - point_size/2,
                0
            ],
            size=[point_size, point_size],
            opacity=0
        )


    @fail_gracefully()
    def on_foreground_drawn(self,*args, **kwargs):

        # Reveal the rendered rectangles
        animation = Animation(opacity=1, t='in_out_circ', duration=1)
        for graphic, y in self.rendered_graphics:
            graphic.opacity = 0
            new_pos = [
                graphic.pos[0],
                y+self.drawable_bottom-graphic.size[1]/2
            ]
            graphic.pos = new_pos
            animation.start(graphic)

class WeightLineGraph(LineGraphWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.register_event_type("on_show_edit_popup")
        self.color_order = []

    @fail_gracefully()
    def determine_label_text_for_point(self, point_index, series_dataset, label_text,
        *args, **kwargs):

        raw_label_text = series_dataset.labels[point_index]
        return raw_label_text.replace(
                '_c',
                f"{get_hex_from_color(self.color_order[point_index])}"
            )

    async def on_press_async(self, graphic, *args, **kwargs):
        anim = Animation(opacity=1, duration=1)
        graphic.opacity = .2
        anim.start(graphic)
        self.dispatch('on_show_edit_popup', graphic)

    def on_show_edit_popup(self, *args, **kwargs):
        "Hook"

    @fail_gracefully()
    def render_point_graphic(self, *args, width=None, x = 0, value = None, ref_id = None,
        **kargs):

        point_size = dp(35)
        last_value = getattr(self, 'last_value', None)

        if not last_value or last_value == value:
            icon_str = icon('minus')
            icon_color = colors.rms_dark_blue

        elif last_value < value:
            icon_str = icon('caret-up')
            icon_color = colors.rms_green

        else:
            icon_str = icon('caret-down')
            icon_color = colors.rms_red

        graphic_texture = OpexaLabel(
            text=f"{icon_str}",
            size = [point_size, point_size],
            pos=[
                self.drawable_left + x + (width/2) - point_size/2,
                0
            ],
            color=icon_color,
            opacity=0, valign='middle'
        )
        graphic_texture.texture_update()
        graphic_texture.on_press_async = self.on_press_async
        graphic_texture.ref_id = ref_id

        self.color_order.append(icon_color)
        self.last_value = value
        return graphic_texture
                # return Ellipse(
            # pos=[
            #     self.drawable_left + x + (width/2) - point_size/2,
            #     0
            # ],
        #     size=[point_size, point_size],
        # )
        
