import itertools
import math
from functools import partial
import asyncio

from kivy.uix.widget import Widget
from kivy.properties import ListProperty, NumericProperty, ObjectProperty, StringProperty
from kivy.metrics import dp
from kivy.utils import get_hex_from_color
from kivy.clock import Clock
from kivy.graphics import *
from kivy.lang import Builder
from tiny_applet.utils import fail_gracefully, write_to_log

from opexa_kivy.uix.label import OpexaLabel
from opexa_kivy.utils import get_color_module
from opexa_kivy.fonts import icon


colors = get_color_module()

Builder.load_string("""
#: import GraphSurface opexa_kivy.uix.graphs.surface.GraphSurface

<BaseGraph>:
    GraphSurface:
        id: surface
        size: root.size
        pos: root.pos
    
""")

class BaseGraph(Widget):
    color_pallete = []
    label_color = ListProperty([0, 0, 0, .8])
    color_iterator = itertools.cycle(
        [colors.chart_blue, colors.chart_pink, colors.orange]
    )
    colors = ListProperty([])
    
    top_padding = dp(30)
    right_padding = dp(5)
    bottom_padding = dp(40)
    left_padding = dp(30)
    
    inter_series_spacing = dp(10)
    heading_height = dp(40)
    datasets = ObjectProperty()
    title = StringProperty("")
    legend = ListProperty([])
    legend_font_size = NumericProperty(10)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.series_color_map = {}
        self.register_event_type('on_foreground_drawn')
    
    def render_to_screen(self, *args, **kwargs):
        raise NotImplemented()

    @property
    def surface(self):
        return self.ids['surface']

    @property
    def drawable_height(self):
        return self.surface.height-self.bottom_padding-self.top_padding

    @property
    def drawable_bottom(self):
        return self.drawable_pos[1]

    @property
    def drawable_top(self):
        return self.drawable_pos[1] + self.drawable_height

    @property
    def drawable_right(self):
        return self.drawable_pos[0] + self.drawable_width

    @property
    def drawable_left(self):
        return self.drawable_pos[0]

    @property
    def drawable_width(self):
        return self.surface.width-self.left_padding-self.right_padding

    @property
    def drawable_size(self):
        return (self.drawable_width, self.drawable_height)

    @property
    def drawable_pos(self):
        pos_x = self.pos[0] + self.left_padding
        pos_y = self.pos[1] + self.bottom_padding
        return (pos_x, pos_y)

    @fail_gracefully()
    def guess_chart_label_text(self, *args, **kwargs):
        return []

    @property
    def chart_heading_text(self):
        
        legend_text_lst = []
        font_size = math.floor(self.legend_font_size) # Must be integer
        for i, l in enumerate(self.legend):

            if isinstance(l, tuple):
                raw_text = l[0]
                color_hex = l[1]
            else:
                raw_text = l 
                color_hex = get_hex_from_color(self.colors[i])

            legend_text = (
                f"[size={font_size}][color={color_hex}]"
                f"{icon('square')}[/color] {raw_text}[/size]"
                )
            legend_text_lst.append(legend_text)
        full_legend_text = "   ".join(legend_text_lst)

        return f"{self.title}\n{full_legend_text}"

    @fail_gracefully()
    def build_color_map(self):
        """ Builds the color map used to determine what color each bar should be. """
        for index, series in enumerate(self.datasets):
            self.series_color_map[index] = next(self.color_iterator)

    @fail_gracefully()
    def clear_rendered_graphics(self, *args, **kwargs):
        canvas_list = [
            self.surface.canvas.before, self.surface.canvas, self.surface.canvas.after
        ]
        for c in canvas_list:
            c.clear()

    def draw_title(self, *args, **kwargs):
        title_label = OpexaLabel(
            text=self.chart_heading_text, width=self.surface.width,
            valign ='center', color=self.label_color,
            line_height = .8
        )
        title_label.texture_update()

        with self.surface.canvas.after:
            Color(1,1,1,1)
            Rectangle(
                pos=[self.surface.x, self.surface.top-title_label.texture.size[1]],
                size=title_label.texture.size,
                texture=title_label.texture
            )


    @fail_gracefully()
    def render(self, *args, **kwargs):
        
        if self.series_color_map == {}:
            self.build_color_map()

        Clock.schedule_once(self.clear_rendered_graphics, 0.05)
        Clock.schedule_once(self.draw_title, .15)
        Clock.schedule_once(self.draw_background, 0.09)
        Clock.schedule_once(self.draw_foreground, .2)


class HorizontalBackgroundMixin():

    @fail_gracefully()
    def render(self, *args, **kwargs):
        super().render(*args, **kwargs)

        y_max = 0
        for data_point in self.datasets:
            if not data_point.data:
                continue


            y_max = max(max([float(x) for x in data_point.data]), y_max)

        self.step, self.max_value = self.calculate_y_axis(y_max)

    @fail_gracefully()
    def calculate_y_axis(self, max_value):
        """ Determine the step and rounded up maximum value of the graph. """

        rounded_max = int(math.ceil(max_value / 10.0)) * 10
        if rounded_max == 0:
            rounded_max = 100
            
        if rounded_max <= 10:
            step = 1
        elif rounded_max <=50:
            step = 5
        elif rounded_max <=100:
            step = 10
        else:
            step = int(math.ceil(rounded_max/10/10.0)) * 10
        
        return step, rounded_max

    @fail_gracefully()
    def draw_background(self, *args, **kwargs):

        # First, determine the number of horizontal guides we'll need
        guide_count = int(self.max_value/self.step)
        guides_to_render = [(0, None), ]
        label_textures = []
        for guide_num in range(guide_count):
            val = (guide_num*self.step) + self.step
            if val < self.max_value: # Skips guide line at max

                # Make the texture holding label text
                l = OpexaLabel(
                    text=str(val),
                    width=self.left_padding, halign='center')
                l.texture_update()

                # Append to list of guides
                guides_to_render.append(
                    (val/self.max_value, l.texture)
                )

        with self.surface.canvas.before:            
            
            for y_offset, label_texture in guides_to_render:                    
                guide_y = self.drawable_pos[1] + (self.drawable_height*y_offset)

                # Draw line
                Color(0, 0, 0, .2)
                line = Line(
                    points=[
                        self.drawable_left, guide_y,
                        self.drawable_right, guide_y
                        ]
                )

                # Draw label texture
                Color(*self.label_color)
                if label_texture:
                    line_label = Rectangle(
                        pos=[self.surface.x, guide_y-label_texture.height/2],
                        size=label_texture.size, texture=label_texture)

    def value_to_y_pos(self, value, *args, **kwargs):
        return (self.drawable_height)*(value/self.max_value)

    def on_touch_down(self, touch, *args):
        if super().on_touch_down(touch, *args):
            return True

        for graphic, y_pos in self.rendered_graphics:

            if not (graphic.pos[0] < touch.pos[0] and
                graphic.pos[0] + graphic.size[0] > touch.pos[0]):
                continue
            if (graphic.pos[1] < touch.pos[1] and
                graphic.pos[1] + graphic.size[1] > touch.pos[1]):

                if hasattr(graphic, 'on_press_async'):
                    asyncio.create_task(
                        graphic.on_press_async(graphic)
                    )
                return True

    @fail_gracefully()
    def draw_foreground(self, *args, **kwargs):
        self.rendered_graphics = []
        self._series_labels_rendered = {}

        label_text = self.guess_chart_label_text()
        with self.surface.canvas:
            
            total_series = len(self.datasets)
            for series_index, series_dataset in enumerate(self.datasets):
                
                total_points = len(series_dataset.data)
                for point_index, point in enumerate(series_dataset.data):
                    point = float(point)
                    point_y = self.value_to_y_pos(point)
                    point_x, point_width, series_x, series_width = self.pos_index_to_x(
                        series_index, point_index,
                        total_series=total_series, total_points=total_points)
                    canvas_color, color_args = self.pos_index_to_color(
                        series_index, point_index, series_dataset,
                        total_series=total_series, total_points=total_points)

                    try:
                        ref_id = series_dataset.ids[point_index]
                    except IndexError:
                        ref_id = None

                    point_graphic = self.render_point_graphic(
                        width=point_width, x=point_x, y=point_y,
                        value=point, return_color=True, ref_id=ref_id
                    )
                    self.rendered_graphics.append((point_graphic, point_y))

                    # Scheduled to render at some random interval in the near future.
                    Clock.schedule_once(
                        partial(
                            self.render_series_label, series_dataset, point_index, series_x,
                            series_width, label_text=label_text),
                        0.1
                    )

        self.dispatch('on_foreground_drawn')
        

    @fail_gracefully()
    def pos_index_to_x(self, series_index, point_index,
        total_series=0, total_points=0):

        cumulative_x_offset = self.inter_series_spacing*point_index
        total_inter_series_spacing_x = self.inter_series_spacing*(total_series+1)

        # Convert hints to explicit widths
        point_x_hint = point_index/total_points
        series_x_hint = series_index/total_series
        drawable_width = self.drawable_width-total_inter_series_spacing_x
        series_width = drawable_width/total_points 
        point_width = (series_width/total_series)

        series_x = series_width*point_index + series_x_hint*series_width
        point_x = series_x + cumulative_x_offset
        return point_x, point_width, series_x, series_width

    @fail_gracefully()
    def pos_index_to_color(self, series_index, point_index, series_dataset,
        total_series=0, total_points=0):

        def _choose_from_default_colors(i):
            return self.series_color_map[i]

        c_args = None
        if series_dataset.colors:
            try:
                c_args = series_dataset.colors[point_index]
            except IndexError:
                write_to_log(
                    'Warning: Requested seried index out of color range. '
                    f'Are there enough colors defined for {self}?',
                    level='warning'
                )

        # Choose from a set of explictly defined colors, if any.
        # Use the defaults if not
        elif self.colors:

            try:
                c_args = self.colors[series_index]
            except IndexError:
                write_to_log(
                    'Warning: Requested seried index out of color range. '
                    f'Are there enough colors defined for {self}?',
                    level='warning'
                )

        if not c_args:
            c_args = _choose_from_default_colors(series_index)

        return Color(*c_args), c_args

    @fail_gracefully()
    def determine_label_text_for_point(self, point_index, series_dataset, label_text,
        *args, **kwargs):

        if label_text:
            txt = label_text[point_index]
        else:
            txt = series_dataset.labels[point_index]

        return txt

    @fail_gracefully()
    def render_series_label(self, series_dataset, point_index, series_x, series_width,
        *args, label_text=[], **kwargs):

        if not self._series_labels_rendered.get(point_index):
            txt = self.determine_label_text_for_point(
                point_index, series_dataset, label_text
            )
            series_label = OpexaLabel(
                    text=txt,
                    width=series_width, halign='center', markup=True
            )
            series_label.texture_update()

            label_x = series_x + self.drawable_left
            label_x += self.inter_series_spacing*point_index
            label_y = self.y + (self.bottom_padding/2) - series_label.texture.size[1]/2

            with self.surface.canvas.after:
                rect = Rectangle(
                    pos=[label_x, label_y],
                    size=series_label.texture.size,
                    texture=series_label.texture
                )

            # Mark this label as done so we do not need to draw it again
            self._series_labels_rendered[point_index] = True