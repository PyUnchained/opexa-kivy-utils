import math
import itertools
from functools import partial
import random

from kivy.animation import Animation
from kivy.clock import Clock
from kivy.graphics import *
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.properties import ObjectProperty, StringProperty, ListProperty, NumericProperty
from kivy.utils import get_hex_from_color
from tiny_applet.utils import write_to_log, fail_gracefully

from opexa_kivy.fonts import icon
from opexa_kivy.utils import get_color_module
from .base import BaseGraph, HorizontalBackgroundMixin

colors = get_color_module()

class BarGraphWidget(HorizontalBackgroundMixin, BaseGraph):

    @fail_gracefully()
    def guess_chart_label_text(self, *args, **kwargs):
        label_text = []

        if self.title == 'Overview':
            for index, series_values in enumerate(
                zip(*[ds.data for ds in self.datasets])
                ):

                txt = (
                    f"{self.datasets[0].labels[index]}\n"
                    f"[b][color={colors.rms_dark_blue_hex}]{series_values[0]}[/color][/b]-"
                    f"[b][color={colors.rms_red_hex}]{series_values[1]}[/color][/b]"
                )
                label_text.append(txt)

        return label_text

    @fail_gracefully()
    def on_foreground_drawn(self,*args, **kwargs):

        # Reveal the rendered rectangles
        for rect, rect_y in self.rendered_graphics:
            new_size = [rect.size[0], rect_y]
            animation = Animation(size=new_size, t='in_out_circ', duration=2)
            animation.start(rect)

    @fail_gracefully()
    def render_point_graphic(self, *args, width=None, x = None,  **kargs):
        return Rectangle(
            pos=[self.drawable_left + x, self.drawable_bottom],
            size=(width, 0)
        )

    # @fail_gracefully()
    # def draw_foreground(self, *args, **kwargs):
    #     self._rendered_rect_list = []
    #     self._series_labels_rendered = {}

    #     label_text = self.guess_chart_label_text()
    #     with self.surface.canvas.after:
            
    #         total_series = len(self.datasets)
    #         for series_index, series_dataset in enumerate(self.datasets):
                
    #             total_points = len(series_dataset.data)
    #             for point_index, point in enumerate(series_dataset.data):
    #                 point_y = (self.drawable_height)*(point/self.max_value)
    #                 x, width, series_x, series_width = self.pos_index_to_x(
    #                     series_index, point_index,
    #                     total_series=total_series, total_points=total_points)
    #                 self.pos_index_to_color(
    #                     series_index, point_index, series_dataset,
    #                     total_series=total_series, total_points=total_points)
    #                 rect = Rectangle(
    #                     pos=[self.drawable_left + x, self.drawable_bottom],
    #                     size=(width, 0))
    #                 self._rendered_rect_list.append((rect, point_y))

    #                 # Scheduled to render at some random interval in the near future.
    #                 Clock.schedule_once(
    #                     partial(
    #                         self.render_series_label, series_dataset, point_index, series_x,
    #                         series_width, label_text=label_text),
    #                     0.1
    #                 )
                    

    #     # Schedule the animation of the bars
    #     Clock.schedule_once(
    #         partial(self.reveal_bars_animation),0.3
    #     )

