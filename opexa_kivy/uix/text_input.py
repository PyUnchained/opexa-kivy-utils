import asyncio
import copy
from functools import partial

from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.uix.textinput import TextInput
from kivy.properties import StringProperty, ListProperty, NumericProperty, BooleanProperty
from kivy.graphics import Color, Line, Rectangle
from kivy.clock import Clock
from kivy.metrics import dp
from kivy.animation import Animation
from kivy.base import EventLoop
from kivy.app import App

from opexa_kivy.utils import write_to_log

Builder.load_string("""
<PlainTextInput>:
    background_color: 1,1,1,0

<FormTextInput>:
    background_normal: ''
    background_active: ''
    background_color: [0,0,0,0]
    halign: "left"
    valign: 'middle'
    multiline: False
    size_hint_y: None
    height: dp(40)
    font_size: dp(60)

    canvas.before:
        Color:
            rgba: root.font_color

    canvas.after:
        Color:
            rgba: root.border_color
        Line:
            width: 1
            points: self.x, self.y, self.x+self.width, self.y

""")

class FormTextInput(TextInput):
    font_color = ListProperty([0,0,0,0])
    border_color = ListProperty([1,1,1,0])
    active_font_color = ListProperty([0,0,0,0])
    vertical_padding = NumericProperty(35)
    _set_og_color = BooleanProperty(False)      

    def on_focus(self, instance, has_focus):
        # Apply fix to properly reset the "return" key listener
        # on android.
        if not has_focus:
            def _f(*args, **kwargs):
                app = App.get_running_app()
                app.sm._fix_back_button()
            Clock.schedule_once(_f, 0.2)


class PlainTextInput(TextInput):
    pass


# class BaseTextInput(Widget):
#     text = StringProperty('')
#     color = ListProperty([1,1,1,1])
#     font_size = NumericProperty(dp(15))


#     def __init__(self, *args, with_focus=False, **kwargs):
#         self.with_focus = with_focus
#         super().__init__(*args, **kwargs)
#         Clock.schedule_once(self.create_input_widget, 0.1)

#     def create_input_widget(self, *args):
#         self.input_widget = PlainTextInput(
#             size=self.size, pos=self.pos, multiline=False)

#         # Bind repositioning instructions
#         self.bind(pos=self.input_widget.setter('pos'))
#         self.bind(size=self.input_widget.setter('size'))

#         # Bind monitoring of text changes
#         self.input_widget.bind(text=self.setter('text'))

#         # Match color and other design information
#         self.input_widget.color = self.color
#         self.input_widget.font_size = self.font_size
#         self.add_widget(self.input_widget)
#         self.input_widget.focus = self.with_focus

