import datetime
from itertools import chain
from calendar import TextCalendar
from copy import copy
from functools import partial
import asyncio

from dateutil.relativedelta import relativedelta
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.lang import Builder
from kivy.graphics import Color, Line, Rectangle, Ellipse, SmoothRoundedRectangle
from kivy.clock import Clock
from kivy.metrics import dp
from kivy.properties import ObjectProperty, ColorProperty
from kivy.animation import Animation
from tiny_applet.utils import write_to_log, fail_gracefully
from tiny_applet.utils import TimedCode
from tiny_applet import settings

from opexa_kivy.uix.label import OpexaLabel
from opexa_kivy.utils import get_color_module
colors = get_color_module()


Builder.load_string("""
<Calendar>:
	orientation: "vertical"
	pos_hint: {'center_x':.5, "center_y":.5}
	size_hint_y: None
	height: self.minimum_height

	BoxLayout:
		orientation: "horizontal"
		size_hint_y: None
		height: self.minimum_height

		OpexaButton:
			text: "<"
			size_hint_x: .25
			on_press: root.prev()

		OpexaLabel:
			id: month_label
			text: ""
			font_size: dp(20)
			color: colors.dark_blue
			bold: True

		OpexaButton:
			text: ">"
			size_hint_x: .25
			on_press: root.next()

	DateGridWidget:
		size_hint_y: None
		height: dp(300)
		parent: root
		id: date_grid
		cols: 7
		spacing: dp(5)

	BoxLayout:
		orientation: "horizontal"
		spacing: dp(3)
		size_hint_y: None
		height: self.minimum_height

		OpexaButton:
			text: "Yesterday"
			size_hint_x: 1/3
			on_press: root.yesterday()
			font_size: settings.FONT_SM

		OpexaButton:
			text: "Today"
			size_hint_x: 1/3
			on_press: root.today()
			font_size: settings.FONT_SM

		OpexaButton:
			text: "Tomorrow"
			size_hint_x: 1/3
			on_press: root.tomorrow()
			font_size: settings.FONT_SM

	Widget:
		size_hint_y: None
		height: dp(20)


""")

class Calendar(BoxLayout):
	displayed_date = ObjectProperty()
	selected_date = ObjectProperty()

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self._calendar = TextCalendar()
		self._calendar.setfirstweekday(6)

	@fail_gracefully()
	def on_displayed_date(self, *args, **kwargs):
		if not self.selected_date:
			self.selected_date = self.displayed_date
		self.ids["date_grid"].draw_calendar()
		self.ids['month_label'].text = self.displayed_date.strftime("%b %Y").upper()


	@fail_gracefully()
	async def async_post_draw_date_label_widget(self, widget, *args, **kwargs):
		pass


	def next(self, *args, **kwargs):
		self.displayed_date = self.displayed_date + relativedelta(months=1)

	def prev(self, *args, **kwargs):
		self.displayed_date = self.displayed_date - relativedelta(months=1)

	@fail_gracefully()
	def today(self, *args, **kwargs):
		today = datetime.date.today()
		if self.selected_date == today:
			call_on_date_selected = True
		else:
			call_on_date_selected = False

		
		if self.displayed_date.month != today.month:
			self.displayed_date = today

		# Setting the selected_date property needs to happen after the
		# displayed date is reset, so that calendar has time to be redrawn
		def __trigger_on_date_selected(*args, **kwargs):
			self.selected_date = today
			if call_on_date_selected:
				self.ids['date_grid'].on_date_selected(
					self, self.today
				)
		Clock.schedule_once(__trigger_on_date_selected, 0)

	@fail_gracefully()
	def tomorrow(self, *args, **kwargs):
		date = datetime.date.today() + relativedelta(days=1)
		if self.displayed_date.month != date.month:
			self.displayed_date = date
		
		# Setting the selected_date property needs to happen after the
		# displayed date is reset, so that calendar has time to be redrawn
		def __set_date(*args):
			self.selected_date = date
		Clock.schedule_once(__set_date, 0)


	def yesterday(self, *args, **kwargs):
		date = datetime.date.today() - relativedelta(days=1)
		if self.displayed_date.month != date.month:
			self.displayed_date = date
		
		# Setting the selected_date property needs to happen after the
		# displayed date is reset, so that calendar has time to be redrawn
		def __set_date(*args):
			self.selected_date = date
		Clock.schedule_once(__set_date, 0)

class DateGridWidget(GridLayout):
	parent = ObjectProperty()
	highlight_color = ColorProperty(colors.calendar_highlight)
	highlight_text_color = ColorProperty([1,1,1,1])
	default_color = ColorProperty([0,0,0,1])
	text_color = ColorProperty([0, 0, 0, 1])

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.date_select_widgets = []
		self._today_widget = None
		self.__selected_widget = None
		self.register_event_type("on_double_tap_event")
		self.register_event_type("on_single_tap_event")


	def on_double_tap_event(self, *args, **kwargs):
		pass

	def on_single_tap_event(self, *args, **kwargs):
		pass

	def __update_today_indicator_pos(self, graphic, label_widget, pos, *args, **kwargs):
		graphic.pos = pos

	def __update_today_indicator_size(self, graphic, label_widget, size, *args, **kwargs):
		graphic.size = size

	def is_today(self, date):
		if not date:
			return False
		return date == datetime.datetime.now().date()

	def is_selected(self, date):
		if not date:
			return False
		return date == self.parent.selected_date


	@fail_gracefully()
	def remove_all_highlighting(self, *args, **kwargs):
		for date_widget in self.date_select_widgets:
			Animation.cancel_all(date_widget)
		self.remove_date_selected_highlight()
		self.remove_schedule_highlight()
		self.remove_today_highlight()


	@fail_gracefully()
	def remove_schedule_highlight(self, *args, **kwargs):
		for date_widget in self.date_select_widgets:
			if date_widget == self._today_widget:
				continue

			date_widget.color = copy(date_widget.default_color)
			date_widget.font_size = settings.FONT_MD
			date_widget.ellipse_color = colors.transparent
			date_widget.bold = True


	@fail_gracefully()
	def render_event_highlight(self, widget, *args, warning_level=0, **kwargs):

		if warning_level == 0:
			ellipse_color = colors.dark_blue
		elif warning_level == 1:
			ellipse_color = colors.orange
		else:
			ellipse_color = colors.red

		if warning_level > 0:
			Animation(ellipse_color=ellipse_color, duration=.5).start(widget)
		else:
			widget.ellipse_color = ellipse_color
		widget.color = colors.white
		widget.font_size = settings.FONT_LG


	@fail_gracefully()
	def get_events(self, date, *args, **kwargs):
		try:
			return list(filter(
				lambda x: x.date==date,
				self.parent.calendar_events
			))

		except AttributeError:
			return []


	@fail_gracefully()
	def draw_calendar(self, *args, **kwargs):
		

		# Create the widgets in the grid required to display the
		# calendar text and register user input
		if not self.date_select_widgets:
			self.render_date_select_widgets()

		# Remove any previous label highlighting
		self.remove_all_highlighting()

		calendar_month_days = list(
			self.parent._calendar.itermonthdays(
				self.parent.displayed_date.year,
				self.parent.displayed_date.month
			)
		)

		# Convert calendar days into actual datetime instances to make them
		# easier to work with
		month_dates = []
		for d in calendar_month_days:
			month_dates.append(
				self.text_to_displayed_date(str(d))
			)

		for index, widget in enumerate(self.date_select_widgets):
			remove_highlighting = True

			try:
				date_instance = month_dates[index]
				if not date_instance:
					text_str = ""
				else:
					text_str = str(date_instance.day)

			# Means that this widget is not a part of the current
			# month being displayed, so we want to clear its text
			except IndexError:
				date_instance = None
				text_str = ""

			widget.date_instance = date_instance
			widget.text = text_str


			

			if self.is_selected(date_instance):
				self.render_selected_hightlight(widget)

			if self.is_today(date_instance):
				self._today_widget = widget
				self.render_today_highlight(widget)
				

			events = self.get_events(date_instance)
			if events:
				highest_level = 0
				for e in events:
					lvl = e.lead_time_warning_level(date_instance)
					if lvl > highest_level:
						highest_level = lvl

				self.render_event_highlight(
					widget, warning_level=highest_level
				) 

			asyncio.create_task(
				self.parent.async_post_draw_date_label_widget(
					widget
				)
			)

				

	def calculate_ellipse_pos(self, pos, *args, **kwargs):
		max_size = min(self.col_width, self.row_height)
		size = [max_size, max_size]
		if max_size == self.col_width:
			ellipse_pos = [pos[0]+size[0]/2, pos[1]]
		else:
			ellipse_pos = [
				pos[0]+((self.col_width-size[0])/2), pos[1]
			]
		return ellipse_pos, size


	def generate_texture(self, text_str, *args, **kwargs):
		from opexa_kivy.uix.label import OpexaLabel as CoreLabel
		mylabel = CoreLabel(
			text=text_str, font_size=30, color=(0, 0, 0, 1), size_hint=(None, None),
			size=(self.col_width, self.row_height), halign="center", valign="middle"
		)
		mylabel.refresh()
		return mylabel.texture


	def on_date_selected(self, calendar_widget, selected_date, *args, **kwargs):
		### calendar_widget arg should never be used, as it could actually be anything

		# pass
		for widget in self.date_select_widgets:
			if self.is_selected(self.text_to_displayed_date(widget.text)):
				self.render_selected_hightlight(widget)
			if widget.date_instance == selected_date:
				asyncio.create_task(
					self.parent.async_post_draw_date_label_widget(
						widget
					)
				)


	def on_parent(self, *args, **kwargs):
		self.parent.bind(selected_date=self.on_date_selected)


	def on_touch_down_handler(self, widget, touch, *args, **kwargs):

		if widget.collide_point(*touch.pos):


			try:
				value = int(widget.text)
			except ValueError:
				return
			
			new_date = datetime.date(
				day=value, month=self.parent.displayed_date.month,
				year=self.parent.displayed_date.year
			)
			if new_date == self.parent.selected_date:
				self.render_selected_hightlight(widget)
			self.parent.selected_date = new_date

			if touch.is_double_tap:
				self.dispatch(
					"on_double_tap_event",
					date=self.parent.selected_date,
					calendar_grid_widget=widget
				)
			else:
				self.dispatch(
					"on_single_tap_event",
					date=self.parent.selected_date,
					calendar_grid_widget=widget
				)


	def render_date_select_widgets(self, *args, **kwargs):
		DAY_SYMBOLS = ["S", "M", "T", "W", "T", "F", "S"]
		for i in range(7*7):
			# Write the text for the date headings
			if i < 7:
				text = DAY_SYMBOLS[i]
				append_as_selector = False

			else:
				text = ""
				append_as_selector = True

			label = CalendarLabel(
				text=text, font_size=settings.FONT_MD,
				font_name="Sansation"
			)
			self.add_widget(label)
			label.bind(on_touch_down=self.on_touch_down_handler)

			if append_as_selector:
				self.date_select_widgets.append(label)

	@fail_gracefully()
	def remove_today_highlight(self, *args, **kwargs):
		for date_widget in self.date_select_widgets:
			date_widget.border_color = copy(date_widget.default_border_color)

	@fail_gracefully()
	def render_today_highlight(self, widget, *args, **kwargs):
		widget.border_color = colors.dark_blue

	@fail_gracefully()
	def remove_date_selected_highlight(self, *args, **kwargs):
		for date_widget in self.date_select_widgets:
			if date_widget == self._today_widget:
				continue
			date_widget.border_color = copy(date_widget.default_border_color)


	@fail_gracefully()
	def render_selected_hightlight(self, widget, *args, **kwargs):
		self.remove_date_selected_highlight()
		widget.border_color = colors.light_blue


	def text_to_displayed_date(self, text, *args, **kwargs):
		if text == "0" or text == '':
			return None

		try:
			day = int(text)

		except AttributeError:
			return None

		try:
			return datetime.date(
				day=day, month=self.parent.displayed_date.month,
				year=self.parent.displayed_date.year
			)

		except ValueError:
			return None


Builder.load_string("""
<CalendarLabel>:
	color: self.default_color

	canvas.before:
		Color:
			rgba: self.bg_color
		RoundedRectangle:
			pos: self.pos
			size: self.size
			radius: dp(5),

		Color:
			rgba: self.ellipse_color
		SmoothEllipse:
			pos: self.pos[0]+dp(3), self.pos[1] +dp(3)
			size: self.size[0]-dp(6), self.size[1]-dp(6)

	canvas:
		Color:
			rgba: self.border_color
		Line:
			rounded_rectangle: self.x, self.y, self.width, self.height, dp(5)

""")

class CalendarLabel(Label):
	default_bg_color = ColorProperty(colors.white)
	default_border_color = ColorProperty(colors.white)
	bg_color = ColorProperty(colors.white)
	border_color = ColorProperty(colors.white)
	default_color = ColorProperty(colors.dark_blue)
	ellipse_color = ColorProperty(colors.transparent)

