from copy import copy
from functools import partial
import asyncio

from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.properties import NumericProperty, ListProperty, ObjectProperty, StringProperty, BooleanProperty
from kivy.metrics import dp
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.clock import Clock
from kivy.app import App
from kivy.animation import Animation
from kivy.event import EventDispatcher
from tiny_applet import settings
from tiny_applet.orm.decorators import with_db
from tiny_applet.utils import fail_gracefully, write_to_log
from kivy_gradient import Gradient

from opexa_kivy.uix.button import AcceptButton, RejectButton, DeleteButton
from opexa_kivy.utils import get_color_module


colors = get_color_module()

Builder.load_string("""
#: import dp kivy.metrics.dp

<OpexaPopupWidget>:
	opacity: 0
	size_hint_x: 0.95
	size_hint_y: None
	height: self.minimum_height
	padding: [dp(10), dp(10)]
	orientation: 'vertical'
	spacing: dp(5)

	pos_hint: {"center_x":0.5}

	canvas.before:
		Color:
			rgba: 0, 0, 0, .7

		BoxShadow:
			pos: self.pos
			size: self.size
			offset: 0, 0
			spread_radius: 0, 0
			border_radius: 10, 10, 10, 10
			blur_radius: 30

		Color:
			rgb: self.bg_color

		SmoothRoundedRectangle:
			pos: root.pos
			size: root.size
			radius: [dp(5),]

		Color:
			rgba: self.border_color

		Line:
			width: dp(1)
			rounded_rectangle: self.x, self.y, self.width, self.height, dp(5)






""")

class OpexaPopupBehaviour(EventDispatcher):

	open_delay = NumericProperty(settings.POPUP_OPEN_DELAY)
	close_delay = NumericProperty(settings.POPUP_CLOSE_DELAY)
	calculated_height = NumericProperty(0)    
	timeout = NumericProperty(0)
	shadow_x_offset = NumericProperty(0)
	shadow_y_offset = NumericProperty(0)
	shadow_color = ListProperty([0, 0, 0, .5])
	bg_color = ListProperty([1,1,1,1])
	form = ObjectProperty({})
	popup_title = StringProperty('')
	bg_radius = [dp(0),]
	border_color = ListProperty([0,0,0,.7])
	auto_dismiss = BooleanProperty(True)
	pre_dismiss = ObjectProperty()
	post_dismiss = ObjectProperty()
	post_dismiss_delay = NumericProperty(0)
	debug_id = None
	max_height = 0

	def __init__(self, *args, async_on_success=None, anchor="pos",
		target=None, **kwargs):

		self.anchor = anchor
		self.on_success = kwargs.pop('on_success', None)
		self.async_on_success = async_on_success
		self.fully_open = False
		self.open_anim = None
		self.target = target
		self._target_pos = None
		super().__init__(*args, **kwargs)
		self.register_event_type("on_determine_height")
		self.register_event_type("on_popup_open")
		self.dispatch("on_determine_height")


	@property
	def sm(self):
		app = App.get_running_app()
		return app.sm


	def on_calculated_height(self, instance, height, *args, **kwargs):
		if height != self.height:
			self.height = height


	def on_size(self, instance, size, *args, **kwargs):

		
		if self.fully_open:
			prev_size = getattr(self, 'prev_size', size)
			delta_y = prev_size[1] - size[1]
			if delta_y != 0:
				self.y += delta_y

		# Have to copy it weirdly like this, otherwise calling 'copy' would result
		# in an infinite loop, because it will try to append the 'size' property
		prev_size = copy((size[0], size[1]))
		setattr(self, 'prev_size', prev_size)


	def on_determine_height(self, *args, **kwargs):
		self.calculated_height = self.height


	def on_popup_open(self, *args, **kwargs):
		pass


	@with_db()
	async def post_submit_refresh(self, *args, session=None, **kwargs):
		pass

	def dismiss(self, *args, schedule_dismiss=True, **kwargs):

		if schedule_dismiss:
			Clock.schedule_once(
				partial(self.dismiss, *args, schedule_dismiss=False, **kwargs),
				.15
			)
			return

		if self.pre_dismiss:
			self.pre_dismiss()

		try:
			Window.remove_widget(self)
		except:
			pass

		try:
			self.sm.open_popups.remove(self)
		except ValueError:
			pass

		if self.post_dismiss:
			
			# Determine which function to call. We need it to be synchronous so that
			# we can use Clock.schedule()
			if asyncio.iscoroutinefunction(self.post_dismiss):
				def _f(*args):
					asyncio.create_task(self.post_dismiss())
				post_dismiss_fn = _f

			else:
				post_dismiss_fn = self.post_dismiss
			
			Clock.schedule_once(
				post_dismiss_fn,
				self.post_dismiss_delay
			)


	def adjust_y_pos(self, *args, **kwargs):

		# Adjust the y position to account for the change in height
		final_pos = getattr(self, 'final_pos', None)
		if final_pos:

			# Stop the animation before trying to reposition
			if not self.fully_open:
				open_anim = getattr(self, "open_anim", None)
				open_anim.stop(self)

			new_y = final_pos[1] + (self.max_height - self.calculated_height)
			self.y = new_y  


	def _open(self, pos, *args, **kwargs):

		self.sm.open_popups.append(self)
		Window.add_widget(self)

		def _make_visible(*args, **kwargs):
			self.dispatch("on_popup_open")
			anim = Animation(opacity=1, duration=.1, step=1/24)
			anim.start(self)

		Clock.schedule_once(_make_visible, self.open_delay)


	@property
	def max_y(self, *args, **kwargs):
		return self.sm.top - self.minimum_height - dp(40)


	@fail_gracefully()
	def on_minimum_height(self, *args, **kwargs):
		if self._target_pos:
			self.pos = (
				self._target_pos[0],
				max(0, min(self._target_pos[1], self.max_y))
			)

		else:
			self.pos = (0, max(dp(20), self.max_y))


	def open(self, pos=None, *args, **kwargs):
		# Opens in center of window by default
		if pos:
			self._target_pos = pos

		Clock.schedule_once(
			partial(self._open, pos, *args, **kwargs), 0
		)
		
		if self.timeout:
			Clock.schedule_once(self.dismiss, self.timeout)


	def on_form(self, instance, form, *args, **kwargs):
		""" Add form widget as child. """
		Clock.schedule_once(partial(self._add_form, form), 0)


	def on_touch_down(self, touch, *args, **kwargs):
		""" Ensure that touches are only passed to the registration form fields. """
		# If children should handle the touch
		for c in self.children:
			if c.collide_point(*touch.pos):
				if c.on_touch_down(touch):
					return True

		# Stay open as long as touch was somewhere on popup
		if self.collide_point(*touch.pos):
			return True
		
		# Close if touch occured somewhere other than on the popup
		else:
			if self.auto_dismiss:
				self.dismiss()

			# Keep popup open
			return True            
		

	def post_save(self, *args, **kwargs):
		""" Hook for what actions to take after the submit button is pressed. """
		pass


	def _add_form(self, form, *args, **kwargs):
		try:
			self.add_widget(form)
		except:
			pass


class OpexaPopupWidget(OpexaPopupBehaviour, BoxLayout):
	
	@fail_gracefully()
	def save(self, *args, **kwargs):

		@with_db()
		async def _save(*args, session=None, **kwargs):
			form = self.ids["form"]
			if await form.is_valid(session=session):
				await form.save(session=session)
				self.dismiss()
				
		asyncio.create_task(_save())


Builder.load_string("""
#: import Gradient kivy_gradient.Gradient
#: import icon opexa_kivy.fonts.icon
	
<TitledPopup>:
	heading_color: colors.white
	padding: [dp(10), dp(10), dp(10), dp(20)]

	BoxLayout:
		orientation: "vertical"
		size_hint_y: None
		height: self.minimum_height

		canvas.before:
			Color:
				rgba: colors.white

			RoundedRectangle:
				pos: root.x, self.y
				size: root.width, self.height + root.padding[1]
				radius: [dp(5), dp(5), 0, 0]
				texture: root.heading_texture
	
	
		OpexaLabel:
			id: heading_label
			text: root.heading
			bold: True
			color: root.heading_color
			valign: "middle"

		Widget:
			size_hint_y: None
			height: root.padding[1] if root.heading else settings.FONT_MD + dp(5)



<TextPopup>:
	opacity: 0 if not self.body else 1
	
	OpexaLabel:
		id: body_label
		text: root.body
		color: colors.dark_blue


<FormPopup>:
	auto_dismiss: False

""")




class TitledPopup(OpexaPopupWidget):
	heading = StringProperty("")
	heading_y = NumericProperty(0)

	level = NumericProperty(10)
	heading_texture = ObjectProperty(None)
	heading_height = NumericProperty(0)


	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.set_heading_color()



	def on_level(self, *args, **kwargs):
		if self.level <= 20 and self.level > 10 and self.heading == "":
			self.heading = "Warning"
		elif self.level > 20 and self.heading == "":
			self.heading = "Error"
		self.set_heading_color()


	def set_heading_color(self, *args, **kwargs):
		heading_color = colors.heading_blue
		if self.level < 0:
			heading_color = colors.heading_green
		elif self.level <=20 and self.level > 10:
			heading_color = colors.heading_orange
		elif self.level > 20:
			heading_color = colors.heading_red
		self.heading_texture = Gradient.horizontal(*heading_color)



	# def position_heading_background(self, widget=None, *args, **kwargs):
	# 	if not widget:
	# 		widget = self.ids.get('heading_label', None)
	# 		if not widget:
	# 			return

	# 	self.heading_y = widget.y
	# 	self.heading_height = self.top - self.heading_y


class TextPopup(TitledPopup):
	body = StringProperty("")

class SubmitPopup(TitledPopup):
	submit_fn = ObjectProperty()
	model_class = ObjectProperty()
	accept_btn_only = BooleanProperty(False)
	can_delete = BooleanProperty(False)


	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		Clock.schedule_once(self.render_submit_buttons, 0)

	def render_submit_buttons(self, *args, **kwargs):
		if not self.can_delete:
			yes_pos_hint = {'center_x':1/4, 'center_y':.5}
			no_pos_hint = {'center_x':3/4, 'center_y':.5}
			delete_pos_hint = None

		else:
			yes_pos_hint = {'center_x':1/5, 'center_y':.5}
			no_pos_hint = {'center_x':2.5/5, 'center_y':.5}
			delete_pos_hint = {'center_x':4/5, 'center_y':.5}

		self.yes_btn = AcceptButton(
			popup=self, pos_hint=yes_pos_hint
		)
		self.no_btn = RejectButton(
			popup=self, pos_hint=no_pos_hint
		)
		
		holder = FloatLayout(
			size_hint=(1, None), height=settings.FONT_LG
		)
		self.add_widget(holder)
		holder.add_widget(self.yes_btn)

		if not self.accept_btn_only:
			holder.add_widget(self.no_btn)

			if self.can_delete:
				self.delete_btn = DeleteButton(
					popup=self, pos_hint=delete_pos_hint,
					callable_fn=self.delete_obj
				)
				holder.add_widget(self.delete_btn)

		else:
			yes_pos_hint['center_x'] = .5


	@with_db()
	async def create_and_save(self, form, *args, session=None, **kwargs):
		if not self.model_class:
			write_to_log(
				f"Cannot Save! Model class not defined on {self.__class__}",
				level='warning'
			)
			return

		instance_kwargs = copy(form.change_cache)
		if hasattr(self, "ai"):
			instance_kwargs['account_id'] = self.ai._account_uid

		obj = self.model_class(**instance_kwargs)
		session.add(obj)
		await session.commit()
		await session.refresh(obj)	# Ensures join relationships update


class FormPopup(SubmitPopup):
	pass

