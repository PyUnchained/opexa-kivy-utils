from copy import copy
from functools import partial

from kivy.animation import Animation
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import StringProperty, ListProperty, ObjectProperty, ColorProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from opexa_kivy.fonts import icon
from opexa_kivy.uix.label import OpexaLabel
from opexa_kivy.uix.popup import OpexaPopupWidget
from opexa_kivy.uix.recycleview.data_views import RecycleDataViewBaseBehavior
from opexa_kivy.uix.recycleview.core import OpexaRecycleView
from opexa_kivy.utils import get_color_module
from tiny_applet.utils import write_to_log, fail_gracefully

colors = get_color_module()

Builder.load_string("""
#: import Gradient kivy_gradient.Gradient

<MenuBar>:
	oriantation: "horizontal"
	size_hint: (1, None)
	height: settings.MENU_HEIGHT
	spacing: dp(5)

	canvas.before:
		Color:
			rgb: [1,1,1]
		# Rectangle:
		# 	pos: self.pos
		# 	size: self.size
		# 	texture: Gradient.vertical(*colors.blue_gradient)

		Rectangle:
			pos: self.pos[0] + self.width/3, self.pos[1]+self.height/4
			size: 1, self.height/2
			# texture: Gradient.vertical(*colors.white_blue_gradient)

		Rectangle:
			pos: self.pos[0] + 2*self.width/3, self.pos[1]+self.height/4
			size: 1, self.height/2
			# texture: Gradient.vertical(*colors.white_blue_gradient)

	MenuButton:
		text: root.btn_1_text
		on_press: root.get_screen_menu()

	MenuButton:
		text: root.btn_2_text

	MenuButton:
		text: root.btn_3_text
		

<MenuButton>:
	color: colors.white
	font_size: dp(17)
	line_height: 1.2

<MenuBarPopup>:

	OpexaRecycleView:
		id: rv
		viewclass: "BlueMenuDataView"
		default_height: dp(60)
		match_child_height: True

<MenuDataView>:
	font_color: colors.white
	orientation: "horizontal"

	OpexaLabel:
		text: root.icon_text
		size_hint_x: .2
		color: root.font_color
		font_size: settings.FONT_LG

	OpexaLabel:
		text: root.text
		halign: 'left'
		color: root.font_color
		bold: True
		font_size: settings.FONT_LG

<BlueMenuDataView>:
	font_color: colors.dark_blue
""")

class MenuBar(BoxLayout):
	btn_1_text = StringProperty(
		icon("copy") + "\nMenu"
	)
	btn_2_text = StringProperty(
		icon("wrench") + '\nSettings'
	)
	btn_3_text = StringProperty(
		icon("person-circle-question") + '\nHelp'
	)
	sm = ObjectProperty()


	@fail_gracefully()
	def get_screen_menu(self, *args, **kwargs):
		options = []
		for x in self.sm.screen_classes:
			if not x.verbose_name:
				continue

			options.append({
				"text": x.verbose_name,
				"callable_fn":partial(self.sm.dispatch, "on_goto_screen", x.name)
			})

		popup = MenuBarPopup()
		popup.options = options
		popup.open()

class MenuRecycleView(OpexaRecycleView):
	pass


class MenuDataView(RecycleDataViewBaseBehavior, BoxLayout):
	icon_text = StringProperty(icon("caret-right"))
	text = StringProperty("")

class BlueMenuDataView(MenuDataView):
	pass

class MenuButton(ButtonBehavior, OpexaLabel):
	

	def on_press(self, *args, **kwargs):
		super().on_press(*args, **kwargs)
		self.og_color = copy(self.color)
		self.color=colors.gold

	def on_release(self, *args, **kwargs):
		super().on_release(*args, **kwargs)
		anim = Animation(color=self.og_color, duration=.3)
		anim.start(self)


class MenuBarPopup(OpexaPopupWidget):
	options = ListProperty([])

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		Clock.schedule_once(self.bind_rv_listener, 0)

	@fail_gracefully()
	def bind_rv_listener(self, *args, **kwargs):
		self.ids['rv'].bind(on_item_selected=self.handle_item_selection)

	@fail_gracefully()
	def handle_item_selection(self, rv, index, *args, **kwargs):
		callable_fn = self.options[index]["callable_fn"]
		self.dismiss()
		Clock.schedule_once(callable_fn, 0)

	@fail_gracefully()
	def on_options(self, *args, **kwargs):
		self.ids['rv'].data = self.options
		