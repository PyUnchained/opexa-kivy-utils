from kivy.uix.label import Label
from kivy.lang import Builder
from kivy.properties import ListProperty

Builder.load_string("""
<OpexaLabel>:
    markup: True
    halign: 'center'
    valign: 'middle'
    color: 0,0,0,0.9
    size_hint_y: None
    text_size: self.width, None
    font_size: settings.FONT_MD
    height: self.texture_size[1]
    pos_hint: {"center_x":0.5, "center_y":0.5}

""")

class OpexaLabel(Label):
    bg_color = ListProperty([0,0,0,0])