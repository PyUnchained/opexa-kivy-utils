from copy import copy
import functools
import time
import asyncio 

from kivy.properties import  ListProperty, ColorProperty, ObjectProperty, BooleanProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.button import Button
from kivy.animation import Animation
from kivy.lang import Builder
from tiny_applet.utils import fail_gracefully
from kivy.clock import Clock
from tiny_applet.orm.decorators import with_db

from opexa_kivy.uix.label import OpexaLabel
from opexa_kivy.utils import write_to_log, get_color_module

colors = get_color_module()

Builder.load_string("""
<SubmitButton>:
    width: dp(80)
    size_hint_x: None


<AcceptButton>:
    text: icon("circle-check")
    background_color: colors.green


<RejectButton>:
    text: icon("circle-xmark")
    background_color: colors.orange
    on_release: self.close_popup()

<DeleteButton>:
    text: icon("trash")
    background_color: colors.red

""")

class ClickableLabel(ButtonBehavior, OpexaLabel):
    callable_fn = ObjectProperty()
    og_color = ColorProperty()
    default_animation = BooleanProperty(True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        def _f(*args):
            self.og_color = copy(self.color)
        Clock.schedule_once(_f, .1)
        

    def on_press(self, *args, **kwargs):
        super().on_press(*args, **kwargs)

        if self.default_animation:
            Animation.cancel_all(self)
            self.color = colors.gold
            self.opacity = .3

    def on_release(self, *args, **kwargs):
        super().on_release(*args, **kwargs)

        if self.default_animation:
            Animation.cancel_all(self)
            anim = Animation(
                color=self.og_color,
                duration=.3, opacity=1
            )
            anim.start(self)

        if self.callable_fn:
            if asyncio.iscoroutinefunction(self.callable_fn):
                asyncio.create_task(
                    self.callable_fn(
                        pos=self.pos, btn=self
                    )
                )
            else:
                def _f(*args):
                    self.callable_fn(
                        pos=self.pos, btn=self
                    )
                Clock.schedule_once(_f, 0)

class OpexaButton(ClickableLabel):
    border_color = ListProperty([1,1,1,0])
    background_color = ColorProperty(colors.dark_blue)



class SubmitButton(OpexaButton):
    dismiss_delay = 0
    popup = ObjectProperty()


class AcceptButton(SubmitButton):


    def get_all_forms(self, target, ret=None, *args, **kwargs):
        from opexa_kivy.uix.forms.widget import FormWidget
        ret = ret or []
        for c in target.children:
            if issubclass(c.__class__, FormWidget):
                ret.append(c)
            self.get_all_forms(c, ret)
        return ret


    @fail_gracefully()
    async def _handle_submit(self, *args, **kwargs):
        if not hasattr(self, "popup"):
            return

        submit_fn_args = []
        try:
            form = self.get_all_forms(self.popup)[0]            
            if await form.is_valid():
                submit_fn_args.append(form)
            else:
                return
                
        except IndexError:
            pass

        submit_fn = getattr(self.popup, 'submit_fn', None)
        if submit_fn:
            if asyncio.iscoroutinefunction(submit_fn):
                coroutine = submit_fn(*submit_fn_args)
            else:
                async def async_submit(*args, **kwargs):
                    submit_fn(*submit_fn_args)
                coroutine = async_submit()
            asyncio.create_task(coroutine)

        post_submit_fn = getattr(self.popup, 'post_submit', None)
        if post_submit_fn:
            if not asyncio.iscoroutinefunction(submit_fn):
                write_to_log(
                    f"'{post_submit_fn}' method is not asynchronous"
                )
                return
            def __trigger_post_submit(*args):
                asyncio.create_task(post_submit_fn(*submit_fn_args))
            Clock.schedule_once(__trigger_post_submit, .2)

        Clock.schedule_once(self.popup.dismiss, self.dismiss_delay)

    
    @fail_gracefully()
    def on_release(self, *args, **kwargs):
        super().on_release(*args, **kwargs)
        asyncio.create_task(self._handle_submit())


class RejectButton(SubmitButton):
    
    def close_popup(self, *args, **kwargs):
        Clock.schedule_once(self.popup.dismiss, self.dismiss_delay)



class DeleteButton(SubmitButton):
    pass