from kivy_django_restful import kdr_applet

from contrib.utils import write_to_log

def calculate_standard_rv_height(rv, *args,
    label_height=kdr_applet.settings.DEFAULT_DATA_VIEW_HEIGHT, max_length=5, **kwargs):

    max_height = label_height*max_length
    grid_spacing = rv.ids["grid"].spacing[0]
    calculated_height = len(rv.data) * label_height
    return min(max_height, calculated_height)