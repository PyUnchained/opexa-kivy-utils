import asyncio
import copy
from functools import partial
from math import ceil

from kivy.animation import Animation
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import ObjectProperty, NumericProperty, StringProperty, ListProperty, BooleanProperty
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from tiny_applet import settings
from tiny_applet.utils import write_to_log
from kivy.graphics.texture import Texture

from opexa_kivy.uix.label import OpexaLabel
from opexa_kivy.fonts import icon
from opexa_kivy.utils.colors import get_color_module

colors = get_color_module()

Builder.load_string("""

<OpexaRecycleView>:
    scroll_distance: dp(5)


    canvas.after:
        Color:
            rgba: [1, 1, 1, self.no_result_texture_opacity]
        Rectangle:
            pos: self.center[0] - self.no_result_texture_size[0]/2, self.center[1] - self.no_result_texture_size[1]/2
            size: self.no_result_texture_size
            texture: self.no_result_texture

    grid_spacing: dp(5)

    RecycleGridLayout:
        id: grid
        default_size: None, root.default_height
        default_size_hint: 1, None
        size_hint_y: None
        height: self.minimum_height
        cols: root.grid_cols
        spacing: root.grid_spacing
        padding: [dp(3), dp(5)]
""")


class SelectionAwareRecycleViewBehaviour():

    default_height = NumericProperty(settings.DEFAULT_DATA_VIEW_HEIGHT)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.register_event_type('on_item_selected')


    def on_item_selected(self, index, *args, touch=None, display_dropdown=False, **kwargs):
        """ Hook for responding to the selection of an item in the list. """

        # Handle simply displaying a dropdown on press
        if display_dropdown:
            from opexa_kivy.uix.dropdown import DropDown
            dropdown = DropDown(options=self.data[index]['options'])
            dropdown.open(touch.pos)
            return

        # Perform a more complicated action defined by a custom method call
        handler_func = self.data[index].get("on_select")
        if handler_func:
            func_call_args = [index, self, touch]

            # Handle asyncio calls and coroutines
            if asyncio.iscoroutinefunction(handler_func):
                asyncio.create_task(handler_func(*func_call_args))

            elif asyncio.iscoroutine(handler_func):
                async def await_wrapper(wrapped):
                    return await wrapped
                asyncio.create_task(await_wrapper(handler_func))
            
            # Synchronous handler
            else:
                handler_func(*func_call_args)

class NoResultsFoundRVMixin():

    no_result_texture = ObjectProperty(Texture.create(size=(1,1)))
    no_result_texture_size = ObjectProperty((10,10))
    no_result_texture_opacity = NumericProperty(0)


    def __init__(self, *args, **kwargs):
        self.show_no_result_texture_anim = Animation(
            no_result_texture_opacity=1, duration=.3
        )
        super().__init__(*args, **kwargs)
        Clock.schedule_once(self.render_no_result_texture, 0.3)


    def render_no_result_texture(self, *args, **kwargs):
        icon_text = icon("magnifying-glass-minus")
        l = OpexaLabel(
            text=f"Not Found\n[color={colors.red_hex}]{icon_text}[/color]",
            font_size=settings.FONT_LG, size=self.size, color=colors.pale_grey
        )
        l.texture_update()

        self.no_result_texture_size = l.texture.size
        self.no_result_texture = l.texture


    def show_no_results(self, *args, **kwargs):
        self.show_no_result_texture_anim.start(self)


    def hide_no_results(self, *args, **kwargs):
        self.show_no_result_texture_anim.stop(self)
        self.no_result_texture_opacity = 0




class OpexaRecycleView(SelectionAwareRecycleViewBehaviour, NoResultsFoundRVMixin, RecycleView):
    """ RecycleView with a custon  'on_item_selected' event fired whenever the user
    selected an option from the list. Designed to be used in tandem with data views
    that inherit from 'SelectableDataViewBehavior' """
    match_child_height = BooleanProperty(False)
    grid_cols = NumericProperty(1)
    grid_spacing = NumericProperty(0)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        Clock.schedule_once(self.adjust_for_child_height, 0)


    def adjust_for_child_height(self, *args, **kwargs):
        if self.match_child_height:
            Clock.schedule_once(self.bind_to_grid_height, 0)

    def bind_to_grid_height(self, *args, **kwargs):
        self.size_hint = (1, None)
        self.height = self.ids["grid"].height

class StripedRecycleView(SelectionAwareRecycleViewBehaviour, RecycleView):
    pass


class FlexibleRecycleView(OpexaRecycleView):

    min_height = NumericProperty(dp(0))
    max_height = NumericProperty(dp(300))


    def on_data(self, *args, **kwargs):
        if not self.data:
            self.height = self.min_height
            return
            
        if self.grid_cols == 1:
            row_count = len(self.data)
        else:
            row_count = ceil(len(self.data)/self.grid_cols)
        height_to_fit_everything = row_count * (self.default_height + self.grid_spacing)
        height_to_fit_everything += self.grid_spacing

        # If the RV has a grid
        height_to_fit_everything = height_to_fit_everything or self.default_height
        if height_to_fit_everything > self.max_height:
            height_to_set = self.max_height
        else:
            height_to_set = height_to_fit_everything
        if height_to_set < self.min_height:
            height_to_set = self.min_height
        self.height = height_to_set
        self.height = self.height
