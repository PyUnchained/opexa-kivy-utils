from functools import partial
from copy import copy
import asyncio

from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.properties import NumericProperty, ListProperty, ColorProperty, ObjectProperty
from kivy.clock import Clock
from kivy.uix.label import Label
from kivy.lang import Builder
from kivy.animation import Animation
from tiny_applet.utils import write_to_log, fail_gracefully

from opexa_kivy.uix.label import OpexaLabel
from opexa_kivy.utils.colors import get_color_module

colors = get_color_module()


Builder.load_string("""
<OpexaLabelDataView>
    color: [0,0,0,.9]
    markup: True

    canvas:
        Color:
            rgba: colors.green
        Line:
            width: dp(1)
            points: self.x, self.y, self.width, self.y

<OpexaDropdownLabelDataView>:
    halign: "center"
    height: settings.DEFAULT_DATA_VIEW_HEIGHT


<RecycleDataViewBaseBehavior>:
    
    canvas:
        Color:
            rgb: [0,0,0]
            a: 0 if self.index % 2 else 0.05
        Rectangle:
            pos: self.pos
            size: self.size

""")

class RecycleDataViewBaseBehavior(RecycleDataViewBehavior):
    index = NumericProperty(0)
    font_color = ColorProperty([0,0,0,1])
    callable_fn = ObjectProperty()
    
    @property
    def recycleview(self):
        return self.parent.parent

    @fail_gracefully()
    def refresh_view_attrs(self, rv, index, data):
        self.index = index
        self.data = data
        super().refresh_view_attrs(rv, index, data)

    @fail_gracefully()
    def on_touch_down(self, touch):
        
        if super().on_touch_down(touch):
            return True

        if self.collide_point(*touch.pos):
            self.og_color = copy(self.font_color)
            self.font_color = colors.gold
            self.on_selected(touch)
            return True

    @fail_gracefully()
    def on_touch_up(self, *args, **kwargs):
        super().on_touch_up(*args, **kwargs)

        # May not be set, especially if touch occured on some other widget
        if hasattr(self, "og_color"):
            anim = Animation(
                font_color=self.og_color,
                opacity=1, duration=.3
            )
            anim.start(self)

    @fail_gracefully()
    def on_selected(self, touch, *args, skip_animation=False, **kwargs):
        try:
            if self.callable_fn:
                if not skip_animation:
                    Animation.cancel_all(self)
                    self.opacity = .6
                    anim = Animation(duration=.5, opacity=1)
                    anim.start(self)

                if asyncio.iscoroutinefunction(self.callable_fn):
                    asyncio.create_task(
                        self.callable_fn(
                            pos=touch.pos,
                            data=self.data
                        )
                    )
                else:
                    Clock.schedule_once(
                        partial(
                            self.callable_fn,
                            pos=touch.pos,
                            data=self.data
                        ),
                            0
                    )
            self.recycleview.dispatch('on_item_selected', self.index)

        except Exception:
            write_to_log("Error", level='error')

class OpexaLabelDataView(RecycleDataViewBaseBehavior, OpexaLabel):
    border_color = ColorProperty([0,0,0,0])
    normal_border_color = ColorProperty([0,0,0,0])

    @fail_gracefully()
    def refresh_view_attrs(self, rv, index, data):
        super().refresh_view_attrs(rv, index, data)

        # Hide the bottom border when there's only one item
        if len(rv.data) <= 1:
            self.border_color = [0,0,0,0]
        else:
            self.border_color = self.normal_border_color

    def on_selected(self, *args, **kwargs):
        # self.press_animation.start(self)
        Clock.schedule_once(
            partial(super().on_selected, *args), 0)

class OpexaDropdownLabelDataView(OpexaLabelDataView):

    @fail_gracefully()
    def on_selected(self, touch,  *args):
        # Touch co-ordinates are currently relative to the RV, so we
        # need to transform them to match window co-ordinates
        touch.push()
        touch.apply_transform_2d(self.to_window)

        self.recycleview.dispatch(
            "on_item_selected", self.index, touch=touch, display_dropdown=True
        )

        touch.pop() # Revert back to the original co-ordinates