from functools import partial
import asyncio
import traceback
import os

from kivy.app import App
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.animation import Animation
from kivy.uix.screenmanager import ScreenManager, FadeTransition
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.utils import platform
from kivy.properties import BooleanProperty, ListProperty, NumericProperty, ObjectProperty
from kivy.base import EventLoop
from tiny_applet import settings
from tiny_applet.utils import write_to_log, fail_gracefully

from opexa_kivy.uix.popup import OpexaPopupWidget


Builder.load_string("""
#: import OpexaButton opexa_kivy.uix.button.OpexaButton
#: import Window kivy.core.window.Window

<OpexaScreenManager>:
    orientation: 'vertical'
    size_hint: (1,1)
    spacing: dp(0)

    canvas.before:
        Color:
            rgba: self.background_color
        Rectangle:
            size: self.size
            pos: self.pos

    FloatLayout:
        id: screen_holder
        size_hint: 1, 1

""")

class OpexaScreenManager(BoxLayout):
    history = []
    timeout_duration = 30
    background_color = [1,1,1,1]
    is_busy = BooleanProperty(False)
    open_popups = ListProperty([])
    toolbar_opacity = NumericProperty(0)
    menu_size_hint = ObjectProperty([1,0])

    @property
    def app(self):
        return App.get_running_app()



    @property
    def current_screen(self):
        return getattr(self, '_current_screen', None)



    def __init__(self, *args, **kwargs):

        if not hasattr(self, "screen_classes"):
            write_to_log(
                "Incorrectly configured ScreenManager class!\n"
                "This might meand that the 'screen_classes' attribute "
                "was not set before this __init__ method ran.",
                level = "error"
            )
            
        self.first_screen_loaded = False
        self.post_init_awaitables = []
        super().__init__(*args, **kwargs)
        self.__screen_cache = {}
        self.register_event_type('on_app_resume')
        self.register_event_type('on_goto_screen')

        if settings.DEBUG:
            self.__asyncio_timeout = 2
        else:
            self.__asyncio_timeout = 10

        write_to_log("Disabled applet notification manager", level='warning')
        self.sanity_check()

             
    def _fix_back_button(self,*args):
        if platform == 'android':
            from jnius import autoclass
            from android.runnable import run_on_ui_thread

            @run_on_ui_thread
            def perform_fix(*args, **kwargs):
                activity = autoclass('org.kivy.android.PythonActivity').mActivity
                activity.onWindowFocusChanged(False)
                activity.onWindowFocusChanged(True)

            perform_fix()


    def _return_key_listener(self, window, key, *args):
        
        # "Return" key pressed
        if key == 27:
            
            # Either close the last opened popup...
            for c in window.children:
                if issubclass(c.__class__, OpexaPopupWidget):
                    if c.auto_dismiss:
                        c.dismiss()
                    return True

            # Or navigate to the previous screen
            return self.go_back()


    def bind_keyboard_listener(self, *args, **kwargs):
        EventLoop.window.bind(on_key_down=self._return_key_listener)


    def clear_history(self, *args, **kwargs):
        self.history = []


    def go_back(self, *args, **kwargs):
        """ Go to the previously opened screen. """

        screen_history_used = self.current_screen.retrace_history()
        if not screen_history_used:
            if len(self.history) > 2:
                self.history = self.history[:-1]
                self.dispatch(
                    'on_goto_screen',
                    self.history[-1].name,
                    ignore_in_history=True
                )

                return True
                
        return False


    def handle_notification(self, screenmanager, *args, **kwargs):
        popup_message(*args, widget=self.notification_widget, **kwargs)


    def on_app_resume(self, *args, **kwargs):
        pass


    def on_current(self, screenmanager, screen_name, *args, **kwargs):
        """ Keeps track of the order in which screens were opened in a list. """
        super().on_current(screenmanager, screen_name, *args, **kwargs)
        if screen_name not in self.history and screen_name != 'login_screen':
            self.history.append(screen_name)


    @fail_gracefully()
    def get_screen(self, name, *args, **kwargs):
        return self.__screen_cache[name]

    @fail_gracefully()
    def on_goto_screen(self, name, *args, ignore_in_history=False, **kwargs):
        self.is_busy = True

        # First check if the screen already exists in the cache
        screen = self.__screen_cache.get(name, None)

        # Create a new instance if None
        if not screen:
            screen_cls = None
            for c in self.screen_classes:
                if c.name == name:
                    screen_cls = c

            if not screen_cls:
                write_to_log(
                    f'Failed to locate a screen_cls for {name}: '
                    f'{self.screen_classes}', level='warning'
                )
                return

            screen = screen_cls(self, opacity=0)
            self.ids['screen_holder'].add_widget(screen)
            self.__screen_cache[name] = screen
            self.first_screen_loaded = True

        if screen == self.current_screen:
            return

        screen.dispatch('on_pre_enter', *args, **kwargs)
        self.show_screen(screen)
        setattr(self, '_current_screen', screen)
        if not ignore_in_history or screen.__class__.ignore_in_history:
            self.history.append(screen)


    def on_kv_post(self, *args, **kwargs):
        self.bind_keyboard_listener()
        
        # Load and display the first screen
        try:
            target_screen_class = self.screen_classes[0]
            Clock.schedule_once(
                partial(self.dispatch, 'on_goto_screen', target_screen_class.name), 0
            )
        except IndexError:
            write_to_log("Failed to find any screen classes associated with this "
                "manager. Was the 'screen_classes' variable set?", level='warning')


    def on_open_popups(self, instance, open_popups, *args, **kwargs):
        pass


    def rebind_keyboard_listener(self, *args, **kwargs):
        self.unbind_keyboard_listener()
        self.bind_keyboard_listener()


    def recv_notification(self, nm, text, *args, level='info', timeout=5,
        font_size=settings.FONT_MD, **kwargs):
        """ Listens for notifications being generated by the kdr_client and displays
        them accordingly to the user, reusing the same widget for efficiency. """

        # Either we'll already have a reference to the latest created popup
        # or we need to create a new one
        nw = getattr(self, 'notification_widget', None)
        if not nw:
            nw = MsgPopup(self, font_size=font_size)
            setattr(self, 'notification_widget', nw)
            Window.add_widget(nw,-1)
        nw.update(text, level, timeout)


    def sanity_check(self, *args, **kwargs):
        """ Makes sure that the manager and its screens are set up correctly. """

        for s in self.screen_classes:
            if not s.name:
                write_to_log(
                    f"Warning: The {s} screen does not declare a name as a "
                    "class level attribute.",
                    level='warning')


    def show_screen(self, screen, *args, **kwargs):

        screen.opacity = 0
        
        def _display_screen(*args, **kwargs):

            # Position other screen out of view
            for name, other_screen in self.__screen_cache.items():
                if screen != other_screen:
                    other_screen.pos = [self.width*5, 0]
                    other_screen.opacity = 0

            screen_holder = self.ids["screen_holder"]
            screen.pos = screen_holder.pos
            screen.opacity = 1

        Clock.schedule_once(_display_screen, 0)


    def unbind_keyboard_listener(self, *args, **kwargs):
        EventLoop.window.unbind(on_key_down=self._return_key_listener)
