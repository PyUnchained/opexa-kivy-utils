from functools import partial
from pathlib import Path
import asyncio

from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.recycleview import RecycleView
from kivy.uix.screenmanager import Screen, ScreenManagerException
from tiny_applet import settings
from tiny_applet.utils import write_to_log

from opexa_kivy.uix.recycleview import OpexaRecycleView
from opexa_kivy.utils import get_color_module

colors = get_color_module()

Builder.load_string("""

<LightScreen>:
    orientation: 'vertical'
    size_hint: 1, 1

    canvas.before:
        Color:
            rgba: self.background_color
        Rectangle:
            size: self.size
            pos: self.pos

""")

class LightScreen(BoxLayout):
    debug_id = None
    name = ''
    background_color = colors.transparent
    verbose_name = ""
    ignore_in_history = False

    def __init__(self, manager, *args, **kwargs):
        self.manager = manager
        super().__init__(**kwargs)
        self.register_event_type('on_enter')
        self.register_event_type('on_pre_enter')
        self.history = []


    def __str__(self):
        return f"<Screen '{self.name}'>"


    def clear_history(self, *args, **kwargs):
        self.history = []


    @property
    def app(self):
        return App.get_running_app()


    @property
    def sm(self):
        return self.manager


    async def async_goto_screen(self, screen_name, *args, wait=0, **kwargs):
        await asyncio.sleep(wait)
        self.manager.current = screen_name


    def on_enter(self, *args, **kwargs):
        pass


    def on_pre_enter(self, *args, **kwargs):
        pass


    def goto_screen(self, name, *args, **kwargs):
        """ Deprecated """
        pass


    def goto_generic_object_screen(self, *args, **kwargs):
        """ Shortcut to display a given object's form on screen. """
        
        generic_object_screen = self.manager.get_screen('generic_object_screen')
        generic_object_screen.display_object(*args, **kwargs)

        
    def retrace_history(self, *args, **kwargs):
        if len(self.history) > 1:
            self.history = self.history[:-1]
            last_call = self.history[-1]
            last_call()
            return True

        #Reset all history, since the app will move to a new screen
        self.history = []
        return False

    
    def get_menu_options(self, *args, **kwargs):
        return []
# class BaseScreen(Screen):
#     background_color = colors.rms_dark_blue
#     screen_transition_delay = 0.1
#     help_doc = 'index'    
#     file_cache_name = 'screen_data_cache.gz'

#     def __init__(self, *args, **kwargs):
#         super().__init__(**kwargs)
#         self.history = []

#     def clear_history(self, *args, **kwargs):
#         self.history = []

#     @property
#     def app(self):
#         return App.get_running_app()

#     async def async_goto_screen(self, screen_name, *args, wait=0, **kwargs):
#         await asyncio.sleep(wait)
#         self.manager.current = screen_name

#     def goto_screen(self, screen_name, *args, **kwargs):
#         """ Changes current screen in separate thread."""
#         Clock.schedule_once(partial(self.__goto_screen, screen_name = screen_name),
#             self.screen_transition_delay)

    
#     def __goto_screen(self, *args, **kwargs):
#         try:
#             self.manager.current = kwargs.get('screen_name', 'home_screen')
#         except ScreenManagerException:
#             write_to_log(f'Failed __goto_screen:\n{args}\n{kwargs}')


#     def goto_generic_object_screen(self, *args, **kwargs):
#         """ Shortcut to display a given object's form on screen. """
#         generic_object_screen = self.manager.get_screen('generic_object_screen')
#         generic_object_screen.display_object(*args, **kwargs)
        
#     def retrace_history(self, *args, **kwargs):
#         if len(self.history) > 1:
#             self.history = self.history[:-1]
#             last_call = self.history[-1]
#             last_call()
#             return True

#         #Reset all history, since the app will move to a new screen
#         self.history = []
#         return False

# class BaseListScreen(BaseScreen):

#     recycleview_kwargs = ObjectProperty({})
#     viewclass = None
#     init_delay = 0 if settings.DEBUG else 0.5
        
#     def add_recycleview(self, *args):
#         write_to_log(self.recycleview_kwargs)
#         self.RV = OpexaRecycleView(**self.recycleview_kwargs)
#         self.RV.viewclass = self.viewclass
#         self.RV.data = self.get_recycleview_data()
#         self.RV.bind(on_item_selected=self.on_item_selected)
#         self.add_widget(self.RV)

#     def on_enter(self, *args):
#         if hasattr(self, 'RV'):
#             Clock.schedule_once(self.refresh, self.init_delay)
#         else:
#             Clock.schedule_once(self.add_recycleview, self.init_delay)

#     def on_item_selected(self, index, *args):
#         pass

#     def get_recycleview_data(self, *args, **kwargs):
#         """ Returns generator used to create the entries in the list"""
#         return []

#     def refresh(self, *args):
#         if hasattr(self, 'RV'):
#             self.RV.data = self.get_recycleview_data()