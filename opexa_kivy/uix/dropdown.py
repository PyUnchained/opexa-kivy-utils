from copy import copy
import functools
import asyncio

from kivy.animation import Animation
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import NumericProperty, ObjectProperty, ListProperty, ColorProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.widget import Widget
from tiny_applet import settings
from tiny_applet.utils import write_to_log

from opexa_kivy.fonts import icon
from opexa_kivy.uix.label import OpexaLabel
from opexa_kivy.uix.popup import OpexaPopupBehaviour, OpexaPopupWidget
from opexa_kivy.utils import get_color_module

colors = get_color_module()

Builder.load_string("""
#: import OpexaRecycleView opexa_kivy.uix.recycleview.OpexaRecycleView
#: import dp kivy.metrics.dp


<DropDown>:
    size_hint: .5, None
    orientation: 'vertical'
    padding: dp(5)

    OpexaRecycleView:
        id: dropdown_rv
        size_hint: 1, None
        grid_cols: 1
        viewclass: "ButtonDataView"
        default_height: dp(50)
        always_overscroll: False
""")

class DropDown(OpexaPopupWidget):
    MAX_HEIGHT = dp(60)*5
    MIN_HEIGHT = dp(60)
    options = ListProperty([])
    animate_scroll_message = BooleanProperty(False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.rv.bind(on_item_selected=self.on_item_selected)
        

    @property
    def rv(self):
        return self.ids['dropdown_rv']


    @property
    def btn_height(self):
        return self.rv.ids["grid"].default_size[1]


    def on_item_selected(self, *args):
        Clock.schedule_once(self.dismiss, self.close_delay)


    def on_options(self, *args, **kwargs):

        data = []
        for opt in self.options:
            data.append({
                'text':f'{opt[0]}',
                'callable_fn':opt[1]
            })

        try:
            self.rv.data = data

        except KeyError:
            def _retry(*args, **kwargs):
                self.dispatch("on_reset_options", self.options)
            Clock.schedule_once(_retry, .2)


        def _fix_height(*args):
            height_to_fit_everything = len(self.rv.data) * (self.rv.default_height + self.rv.grid_spacing)
            height_to_fit_everything += dp(10)
            height_to_fit_everything = height_to_fit_everything or self.rv.default_height
            if height_to_fit_everything > self.MAX_HEIGHT:
                height_to_set = self.MAX_HEIGHT
            else:
                height_to_set = height_to_fit_everything
            if height_to_set < self.MIN_HEIGHT:
                height_to_set = self.MIN_HEIGHT
            self.height = height_to_set
            self.rv.height = self.height
        Clock.schedule_once(_fix_height, 0)
        