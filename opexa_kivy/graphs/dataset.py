class DataSet(object):
    default_color = 'rgba(45, 45, 243, 0.8)'

    def __init__(self, data, *args, labels=[], colors=[], ids=[], **kwargs):
        self.data = data
        self.labels = labels
        self.options = kwargs
        self.colors = colors
        self.ids = ids

class DataPoint(object):

    def __init__(self, value, *args, **kwargs):
        self.value = value