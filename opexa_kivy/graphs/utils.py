from .dataset import DataSet

def create_dataset(*args, **kwargs):
    return DataSet(*args, **kwargs)