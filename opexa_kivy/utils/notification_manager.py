import io

from kivy.event import EventDispatcher
from kivy.core.window import Window

from .core import import_class
from .logging import write_to_log

class TerminalStdOutStream(io.StringIO):
    __doc__ = " Redirects stdout/stderr output and makes it visible to the user\n    through the applet's notification system. "

    def __init__(self, applet, *args, display_to_user = True, timeout=10, **kwargs):
        self.applet = applet
        self.timeout = timeout
        self.display_to_user = display_to_user
        (super().__init__)(*args, **kwargs)

    def write(self, s):

        if self.display_to_user:
            self.applet.notification_manager.notify(
                s, timeout=self.timeout
            )

        else:
            write_to_log(s)

class NotificationManager(EventDispatcher):

    def __init__(self, applet, *args, **kwargs):
        self.applet = applet
        (super().__init__)(*args, **kwargs)
        self.register_event_type('on_notification')

    def notify(self, text, level='info', timeout=5):

        if getattr(self.applet.settings, 'DEBUG', False):
            # 'success' is a special level python doesn't know about, treat it like
            # an normal info call for logging purposes
            python_log_level = level
            if python_log_level == 'success':
                python_log_level = 'info'

            write_to_log(
                f"User Notification - [{level}] :\n{text}",
                level=python_log_level
            )
        self.dispatch('on_notification', text, level=level, timeout=timeout)

    def on_notification(self, *args, **kwargs):
        pass