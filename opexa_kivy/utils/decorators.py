import asyncio
import os
from functools import wraps

from websockets.exceptions import WebSocketException

from tiny_applet.utils import write_to_log

# class fail_gracefully():
#     """ Decorator to help gracefully log exceptions without causing the app to crash. """

#     always_propagate = [WebSocketException]

#     def __init__(self, *args, propagate_errors = [], **kwargs):
#         self.propagate_errors = propagate_errors + self.always_propagate

#     def __call__(self, fn):
#         # Async functions
#         if asyncio.iscoroutinefunction(fn):

#             @wraps(fn)
#             async def async_gracefully_wrapped(*args, **kwargs):

#                 try:
#                     return await fn(*args, **kwargs)

#                 except  Exception as e:
#                     if e in self.propagate_errors or os.getenv('IS_TEST', 0) == 1:
#                         raise e

#                     write_to_log("", level='error')

#             return async_gracefully_wrapped

#         # Sync functions
#         else:

#             @wraps(fn)
#             def gracefully_wrapped(*args, **kwargs):
#                 try:
#                     return fn(*args, **kwargs)
#                 except  Exception as e:
#                     if e in self.propagate_errors or os.getenv('IS_TEST', 0) == 1:
#                         raise e
                        
#                     write_to_log("", level='error')

#             return gracefully_wrapped