from asyncio import iscoroutinefunction
from functools import wraps
import websockets

from opexa_kivy import applet, settings
from opexa_kivy.utils.logging import write_to_log
from opexa_kivy.utils.core import import_class
from .context_managers import WebsocketConnection
from .errors import ServerTimeoutResponse, ServerErrorResponse

class websocket_connection(object):

    def __init__(self, endpoint, protocol='websockets.client.WebSocketClientProtocol'):
        self.endpoint = endpoint
        self.protocol = import_class(protocol)

    def __call__(self, fn):
    
        if not iscoroutinefunction(fn):
            raise ValueError("This wrapper cannot be used with sync functions")

        @wraps(fn)
        async def async_decorated_fn(*args, **kwargs):
            resp = None

            # Determine how the connection will be set up
            connect_kwargs = {"create_protocol":self.protocol}
            if not applet.settings.DEBUG:
                connect_kwargs['ssl'] = create_default_context(
                    cafile=certifi_where()
                )

            # Make the connection, and run the wrapped function
            async with websockets.connect(self.endpoint, **connect_kwargs) as ws:
                try:
                    resp = await fn(ws, applet, *args, **kwargs)
                except Exception as e:
                    write_to_log(e, level='error')

            # Report any error messages returned by the server
            if ws.close_code not in [1000, 1001]:
                msg = (
                    f"Connection closed unexpectedly.\nFunction: {fn}\n"
                    f"Code: {ws.close_code}\nReason: {ws.close_reason}"
                )
                write_to_log(msg, level='warning')

            return resp

        return async_decorated_fn

class sync_websocket_connection(websocket_connection):
    
    def __init__(self, *args, **kwargs):
        kwargs['protocol'] = 'opexa_kivy.utils.websockets.protocols.SyncClientProtocol'
        endpoint = f"{settings.WS_PREFIX}://{settings.WS_HOST}:{settings.WS_PORT}"
        super().__init__(endpoint, *args, **kwargs)
