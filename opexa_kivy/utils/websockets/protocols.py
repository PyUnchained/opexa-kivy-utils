import asyncio
from websockets.client import WebSocketClientProtocol
from websockets.server import WebSocketServerProtocol
from websockets.exceptions import InvalidStatusCode
from pickle import dumps, loads

from opexa_kivy import settings
from opexa_kivy.utils import write_to_log, import_class
from .errors import ServerTimeoutResponse

class SyncRemoteProtocol(WebSocketServerProtocol):

    async def recv(self, *args, **kwargs):
        """ Expects each message sent/received to be in the form of pickled byte data. """
        return loads(await super().recv(*args, **kwargs))

    async def send(self, msg, *args, **kwargs):
        """ Expects each message sent/received to be in the form of pickled byte data. """
        return await super().send(dumps(msg), *args, **kwargs)

class SyncClientProtocol(WebSocketClientProtocol):

    def __init__(self, *args, **kwargs):
        kwargs['extra_headers'] = self.get_auth_headers()
        (super().__init__)(*args, **kwargs)

    async def handshake(self, *args, **kwargs):
        try:
            await super().handshake(*args, **kwargs)
        except InvalidStatusCode as e:
            request = args[0]
            if e.status_code == 500:
                write_to_log(
                    f"API rejected request (are you sure path exists?).\n"
                    f"Host: {request.host}\nPath: {request.path}\n"
                    f"Is Secure: {request.secure}", level='warning')

    def get_auth_headers(self):
        """
        Returns the headers used to authenticate with the server. We just
        need to prove that the request is coming from someone with the correct
        secret key.
        """
        return import_class(settings.WS_HEADER_MANAGER)().get()

    def raise_error_for_msg(self, error, msg):
        error.msg = msg
        raise error

    async def send(self, event, data, *args, **kwargs):
        msg = {'event':event, 'data':data}
        return await super().send(dumps(msg))

    async def recv(self, *args, **kwargs):

        try:
            msg = await asyncio.wait_for(
                (super().recv)(*args, **kwargs),
                timeout=(settings.WS_TIMEOUT)
            )


        # Timeout Occurs
        except asyncio.exceptions.TimeoutError:
            write_to_log(f'Receive Timeout (waited {settings.WS_RECEIVE_TIMEOUT}s)!')

        return loads(msg)