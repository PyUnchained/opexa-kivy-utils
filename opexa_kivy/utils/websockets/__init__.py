from opexa_kivy import applet

class SecureHeaderManager():

    def get(self, *args, **kwargs):
        headers = {'api-key': applet.get_api_key()}
        return headers