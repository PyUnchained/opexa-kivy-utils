class WebsocketError(RuntimeError):
    pass

class ServerErrorResponse(WebsocketError):
    pass

class ServerTimeoutResponse(WebsocketError):
    pass

class EmptyServerResponse(WebsocketError):
    pass