from certifi import where as certifi_where
from copy import copy
from ssl import create_default_context
import websockets

from opexa_kivy import applet
from opexa_kivy.utils.core import import_class
from opexa_kivy.utils.logging import write_to_log

class WebsocketConnection():

    def __init__(self, endpoint, *args, 
        protocol=import_class('websockets.client.WebSocketClientProtocol'),
        **kwargs):

        self.endpoint = endpoint
        self.protocol = protocol

    async def __aenter__(self):
        # If the supplied endpoint doesn't include the base URL
        # include it in the path
        if not any([x in self.endpoint for x in ['ws://','wss://']]):
            self.endpoint = f"{self.get_remote_ws_url()}{self.endpoint}"

        connect_kwargs = {"create_protocol":self.protocol}

        # By default, SSL is off. Turn it on in live environments.
        if not applet.settings.DEBUG:
            connect_kwargs['ssl'] = create_default_context(
                cafile=certifi_where()
            )
            
        self.ws = await websockets.connect(self.endpoint, **connect_kwargs)
        return self.ws

    async def __aexit__(self, exc_type, exc, tb):
        try:
            await self.ws.close()
        except AttributeError:
            pass

    def get_remote_ws_url(self):
        
        url_copy = copy(applet.settings.REMOTE_URL)
        if 'https' in url_copy:
            replace_lookup = 'https'
            replace_with = 'wss'
        else:
            replace_lookup = "http"
            replace_with = 'ws'

        return url_copy.replace(replace_lookup, replace_with) + 'ws/'