from importlib import import_module

from tiny_applet import settings

def get_color_module():
	return import_module(settings.COLOR_MODULE)