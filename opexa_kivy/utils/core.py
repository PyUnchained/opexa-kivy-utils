import importlib
import logging
from os.path import dirname, join, exists, sep, expanduser, isfile
import os

from kivy.utils import platform



def get_user_data_dir(app_name=None):
    """ Based on the '_get_user_data_dir()' method of the 'kivy.opexa_kivy.App' Class. Sometimes
    we need knowledge of the data_dir path even whilst the app itself isn't running. """
    

    if not app_name:
        from opexa_kivy.config.settings import get_app_settings
        app_name = getattr(get_app_settings(), 'APP_NAME', '')

    if platform == 'ios':
        data_dir = expanduser(join('~/Documents', app_name))
    elif platform == 'android':
        from jnius import autoclass, cast
        PythonActivity = autoclass('org.kivy.android.PythonActivity')
        context = cast('android.content.Context', PythonActivity.mActivity)
        file_p = cast('java.io.File', context.getFilesDir())
        data_dir = file_p.getAbsolutePath()
    elif platform == 'win':
        data_dir = os.path.join(os.environ['APPDATA'], app_name)
    elif platform == 'macosx':
        data_dir = '~/Library/Application Support/{}'.format(app_name)
        data_dir = expanduser(data_dir)
    else:
        data_dir = os.environ.get('XDG_CONFIG_HOME', '~/.config')
        data_dir = expanduser(join(data_dir, app_name))
        
    if not exists(data_dir):
        os.mkdir(data_dir)
        
    return data_dir