from asyncio import create_task, sleep
from functools import wraps

class background_thread():
    """ Runs blocking sync functions in a separate thread in a separate thread. """

    def __init__(self, *args, delay=0, **kwargs):
        self.delay = delay

    def __call__(self, fn):

        @wraps(fn)
        async def wrapper(*args, **kwargs):
            await sleep(self.delay)
            return await to_thread(fn, *args, **kwargs)

        return wrapper


def delay_execution(fn, delay, *args, **kwargs):
    
    async def executor():
        await sleep(delay)
        return await fn(*args, **kwargs)

    create_task(executor())

