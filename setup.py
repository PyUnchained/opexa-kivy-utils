import os
from setuptools import setup, find_packages

def read_requirements():
    requirements_list = []
    with open('./requirements.txt') as f:
        for l in f.readlines():
            requirements_list.append(l)
    return requirements_list

setup(
    name='opexa_kivy',
    version='1.0',
    description='Useful Utils for Kivy',
    author='Tatenda Tambo',
    author_email='tatendatambo@gmail.com',
    packages=find_packages(
        exclude=[
        "*.tests", "*.tests.*", "tests.*", "tests", "bin", "build", 'htmlcov', 'include', 'lib'
        ]
    ),
    install_requires = read_requirements()
)